import pandas as pd
import re


class Textreader:
    def __init__(self, mytext):
        self.mytext = mytext.replace("'", "").replace('\r', '').replace(',', '.')
        self.mytext_nomaj = self.mytext.lower()
        print (self.mytext_nomaj)
        bid_offers_input = ['BO', 'request Type', 'CP', 'Origin', 'Prod start year', 'Prod start month',
                            'Prod end year', 'Prod end month', 'COD', 'Volume', 'Price', 'Label', 'Tech', 'Support']

        self.broker_names = ['doug edwards', 'evan redding', 'michael rizzo', 'brian bogusevic', 'nicole shaughnessy', 'seth twisselmann', 'john schiling',
                             'patrick mcfadden', 'nicholas wells', 'zeyd tabbara', 'jake lewis']
        self.trading_team = ['mark scorsolini', 'shane mulqueen']

        self.years_voc = {'2021': '2021', '2022': '2022', '2023': '2023', '2024': '2024','2025': '2025',
                        '2026': '2026', '2027': '2027', '2028': '2028', '2029': '2029', '2030': '2030',
                        '-21': '2021', '-22': '2022', '-23': '2023', '-24': '2024', '-25': '2025', '-26': '2026', '-27': '2027',
                        '-28': '2028', '-29': '2029', '-30': '2030',
                        '21': '2021','22': '2022', '23': '2023', '24': '2024', '25': '2025', '26': '2026', '27': '2027',
                        '28': '2028', '29': '2029', '30': '2030'}

        self.years_list = [2021, 2022, 2023, 2024, 2025, 2026, 2027, 2027, 2028, 2029, 2030, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]

        self.bid_offers = pd.DataFrame(columns=bid_offers_input)

        self.__text_splitter()


    def __text_splitter(self):
        
        # I tried to add code here to remove the broker name and stuff because the program does not seem to do it properly by itself... The stuff we added down here to remove the name of the broker... is not working... 
        # We need to bake a little more logic into this... but how... what do we say and how do we do this... 

        # first we split with timestamp
        self.list_of_quotes = re.split('[0-9]?[0-9]:[0-9][0-9]:[0-9][0-9]\sA?P?M', self.mytext)
        #  we delete our chats
        self.list_of_quotes = [quote for quote in self.list_of_quotes if
                               not any([trader in quote for trader in self.trading_team])]
        # we clean the quotes
        for quote in self.list_of_quotes[:]:
            if 'trades' in quote:
                # self.list_of_quotes.remove(quote)
                continue

            if not self.__is_quote(quote):
                if not self.__is_request(quote):
                    self.list_of_quotes.remove(quote)
                    continue

            # if there is no year in it
            number_of_years = len([i for i in self.years_voc.keys() if str(i) in quote])
            if number_of_years < 1:
                self.list_of_quotes.remove(quote)
                continue

        for quote in self.list_of_quotes[:]:
            # we check if there is multiple years in the quote
            number_of_years = len([i for i in self.years_voc.keys() if str(i) in quote])
            if number_of_years > 1:
                years_found = ['(' + str(i) + ')' for i in re.findall('[0-9][0-9][0-9][0-9]', quote) if i in self.years_voc.keys()]
                self.new_quote = list(filter(bool, quote.splitlines()))
                self.list_of_quotes.remove(quote)
                self.list_of_quotes.extend(self.new_quote)


        '''
        # we split if there is a bid and a offer in the quote
        for quote in self.list_of_quotes:
            if len(re.findall(r'[0-9][0-9].[0-9][0-9]-[0-9][0-9].[0-9][0-9]')) > 0:
        # we first find the year of the quote
        '''

    def __is_request(self, quote):
        if 'request' in quote:
            return True
        if 'looking for' in quote:
            return True
        if 'interest to' in quote:
            return True

        return False

    def __is_quote(self, quote):
        # added @ to check_if_type
        
        # The way that this looks for price is wrong... it looks for numbers with two decimal places... but sometimes the price is an int 
        # with a dollar sign next to it... we should also check for that case... I added this case below... s
        check_if_price = len(re.findall(r'[0-9].[0-9][0-9]', quote)) > 0 or len(re.findall(r'\s?\$[-+]?[0-9]*\.?[0-9]+\s?', quote)) > 0
        check_if_type = any([word in quote for word in ['bid', 'offer', '@']])
        check_if_BO = len(re.findall(r'[0-9].[0-9][0-9]-[0-9].[0-9][0-9]|[0-9].[0-9][0-9][0-9]-[0-9].[0-9][0-9][0-9]|[0-9].[0-9][0-9][0-9]-[0-9].[0-9][0-9]|[0-9].[0-9][0-9]-[0-9].[0-9][0-9][0-9]', quote.replace(' ', ''))) > 0
        
        # The first 'or' below used to be an and... but this works better, I think... just change it back to an and if there are any problems

        if (check_if_price or check_if_type) or check_if_BO:
            return True
        else:
            return False

