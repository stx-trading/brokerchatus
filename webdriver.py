from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
import keyring
import os


class Webdriver:
    def __init__(self,username = os.getlogin()):
        options = Options()
        self.username = username
        #options.add_argument('--headless')
        #options.add_argument('--disable-gpu')
        if os.getlogin() == 'anaveed':
            options.binary_location = r"C:\Users\anaveed\chromedriver.exe" 
        elif os.getlogin() == 'mkim':
            options.binary_location = r'C:\Program Files\Google\Chrome\Application\chrome.exe'
        if username =='smulqueen':
            driver_path = 'C://Users/smulqueen/chromedriver.exe'
        else:
            driver_path = 'C://Users/anaveed/chromedriver.exe' # use format to allow anyone to use this... 
        #chrome_options = webdriver.ChromeOptions()
        #chrome_options.add_argument('--no-sandbox')

        self.__driver = webdriver.Chrome(executable_path=driver_path)
        self.__driver.maximize_window()
        sleep(3)
        self.__login()
        self.__show_more_countries = '//*[@id="origins"]/div[2]/div/button/div'
        self.__show_more_labels = '//*[@id="labels"]/div[2]/button/div'
        self.__show_more_tech = '//*[@id="products"]/div[3]/div/button/div'
        self.__broker_place = '//*[@id="general"]/div[1]/div[2]/div/div'
        self.__year_start_place = '//*[@id="asset-data"]/div[2]/div[1]/div/div[1]/div/div/select'
        self.__year_end_place = '//*[@id="asset-data"]/div[2]/div[2]/div/div[1]/div/div/select'
        self.__month_start_place = '//*[@id="asset-data"]/div[2]/div[1]/div/div[2]/div/div/select'  
        self.__month_end_place = '//*[@id="asset-data"]/div[2]/div[2]/div/div[2]/div/div/select'
        self.__volume_place = '//*[@id="asset-data"]/div[2]/div[5]/div/div/input'
        self.__price_place = '//*[@id="asset-data"]/div[2]/div[6]/div/div/input'
        self.__comment_place = '//*[@id="general"]/div[3]/div/textarea'
        self.__price_request_place = '//*[@id="general"]/div[2]/div[1]/div/div[1]/label/span'
        self.__label_place = '//*[@id="general"]/div[2]/div[1]/div/div[1]/label/span'
        self.__proxy_place ='//*[@id="products"]/div[2]/div/div/div'
        self.__registry_place = '//*[@id="general"]/div[2]/div[2]/div/div/div[2]/input'

        self.comment = 'Indic from Broker [Brokerchat Script]'

        #//*[@id="general"]/div[2]/div[2]/div/div/div[2]
    def __get_new_bidoffer(self):
        self.__driver.get('https://bid-offer-us.datahive.online/#/us_voluntary_us/form')
        #wait for page to load instead
        #WebDriverWait(self.__driver, 100)
        self.__wait_BO_page()


    def __login(self):
        self.__driver.get('https://bid-offer-us.datahive.online/#/login')
        __password = keyring.get_password('datahive', self.username)
        __username = os.getlogin()
        self.__write_value('input', 'data-cy', 'input-username', __username)
        self.__write_value('input', 'data-cy', 'input-password', __password)
        self.__get_value('button', 'data-cy', 'sign-in-button')
        #self.__wait_main_page()
       
    def __get_value(self, type, type_of_text, text_to_find):
        element = self.__driver.find_element_by_xpath('//' + type + '[@' + type_of_text + '="' + text_to_find + '"]')
        self.__driver.execute_script("arguments[0].click();", element)

    def __write_value(self, type, type_of_text, text_to_find, mytext):
        element = self.__driver.find_element_by_xpath('//' + type + '[@' + type_of_text + '="' + text_to_find + '"]')
        element.send_keys(mytext)

    def __get_value_by_xpath(self, xpath):
        element = self.__driver.find_element_by_xpath(xpath)
        self.__driver.execute_script("arguments[0].click();", element)

    def __write_value_by_xpath(self, xpath, mytext):
        element = self.__driver.find_element_by_xpath(xpath)
        element.send_keys(mytext)

    def __get_value_by_xpath_press_enter(self, xpath):
        element = self.__driver.find_element_by_xpath(xpath)
        element.click()
        WebDriverWait(self.__driver, 20)
        element.send_keys(Keys.ENTER)
    
        #FIX, Method not working for counterparty box. Click added as a test method 

    def __wait_main_page(self):
        WebDriverWait(self.__driver, 600).until(EC.visibility_of_element_located(
            (By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div/button[2]/span')))

    def __wait_BO_page(self):
        WebDriverWait(self.__driver, 600).until(EC.visibility_of_element_located((By.XPATH, self.__show_more_countries)))

    def input_quote(self, quote):
        #self.__login() #added for testing, remove when done
        self.__get_new_bidoffer()
        self.__driver.refresh()
        self.__get_new_bidoffer()
        # to show all countries and labels
        self.__get_value_by_xpath(self.__show_more_countries)
        self.__get_value_by_xpath(self.__show_more_labels)
        self.__get_value_by_xpath(self.__show_more_tech)
        # PRICE
        try:
            self.__write_value_by_xpath(self.__price_place, quote.price)
        except:
            pass

        # REQUEST?
#        if quote.price_request:
#            try:
#                self.__get_value_by_xpath(self.__price_request_place)
#            except:
#                pass
        #PROXY
        try:
            self.__get_value_by_xpath(self.__proxy_place)
            self.__write_value_by_xpath(self.__proxy_place, quote.proxy)
            self.__get_value_by_xpath_press_enter(self.__proxy_place)
        except:
            pass
        #COMMENT
        try:
            self.__write_value_by_xpath(self.__comment_place, self.comment)
        except: 
            pass
        # YEAR
        try:
            self.__write_value_by_xpath(self.__year_start_place, quote.start_year)
            self.__write_value_by_xpath(self.__year_end_place, quote.end_year)

        except:
            pass

        # MONTHS
        try:
            self.__write_value_by_xpath(self.__month_start_place, quote.start_month)
            self.__write_value_by_xpath(self.__month_end_place, quote.end_month)
        except:
            pass

        # BROKER
        try:
            self.__get_value_by_xpath(self.__broker_place)
            self.__write_value_by_xpath(self.__broker_place, quote.broker)
            #error press enter
            self.__get_value_by_xpath_press_enter(self.__broker_place)
        except:
            pass

        # TYPE
        try:
            self.__get_value('input', 'value', quote.quote_type)
        except:
            pass

        # ORIGIN
        try:
            self.__get_value('span', 'title', quote.origin)
        except:
            pass

        # SUPPORT
        try:
            self.__get_value('input', 'value', quote.support_type)
        except:
            pass

        # TECH
        try:
            for curr_tech in quote.tech:
                self.__get_value('span', 'title', curr_tech)
        except:
            pass

        # VOLUME
        try:
            self.__write_value_by_xpath(self.__volume_place, quote.volume)
        except:
            pass

        # LABEL
        try:
            for curr_label in quote.label:
                self.__get_value('span', 'title', curr_label)
        except:
            pass

        
        # REGISTRY
        try:
            self.__get_value_by_xpath(self.__registry_place)
            self.__write_value_by_xpath(self.__registry_place, quote.registry)
            print (quote.registry)
            self.__get_value_by_xpath_press_enter(self.__registry_place)
        except:
            pass

        pass 

        #self.__wait_main_page()
        
