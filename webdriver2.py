from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
import keyring
import os
import pandas as pd

credentials = pd.read_csv('cred.csv')

class Webdriver2:
    def __init__(self,username = os.getlogin()):
        options = webdriver.FirefoxOptions()
        self.username = username
        #options.add_argument('--headless')
        #options.add_argument('--disable-gpu')

        if os.getlogin() == 'anaveed':
            options.binary_location = r"C:\Users\anaveed\AppData\Local\Mozilla Firefox\firefox.exe"
        elif os.getlogin() == 'smulqueen':
            options.binary_location = r"C:\Users\smulqueen\AppData\Local\Mozilla Firefox\firefox.exe"
        elif os.getlogin() == 'kborup':
            options.binary_location = r"C:\Program Files\WindowsApps\Mozilla.Firefox_118.0.2.0_x64__n80bbvh6b1yt2\VFS\ProgramFiles\Firefox Package Root\firefox.exe"
        if username =='smulqueen':
            driver_path = 'C://Users/smulqueen/geckodriver.exe'
        elif username == 'kborup':
            driver_path = r"C:\Users\kborup\BitBuck\brokerchatus\geckodriver.exe"
        else:
            driver_path = r"C:\Users\anaveed\Github\brokerchatUS\brokerchatus\geckodriver.exe" # use format to allow anyone to use this...
        #chrome_options = webdriver.ChromeOptions()
        #chrome_options.add_argument('--no-sandbox')

        self.__driver = webdriver.Firefox(executable_path=driver_path, options=options)
        self.__driver.maximize_window()
        sleep(3)
        self.__login()

        self.__show_more_countries = '//*[@id="origins"]/div[2]/div/button/div'
        self.__show_more_labels = '//*[@id="labels"]/div[2]/button/div'
        self.__show_more_tech = '//*[@id="products"]/div[3]/div/button/div'

        self.__broker_place = '//*[@id="general"]/div[1]/div[2]/div/div'

        self.__year_start_place = '//*[@id="asset-data"]/div[2]/div/div[1]/div[1]/div/div[1]/span/div/select'
        self.__year_end_place='//*[@id="asset-data"]/div[2]/div/div[1]/div[2]/div/div[1]/span/div/select'
        self.__month_start_place='//*[@id="asset-data"]/div[2]/div/div[1]/div[1]/div/div[2]/span/div/select'
        self.__month_end_place='//*[@id="asset-data"]/div[2]/div/div[1]/div[2]/div/div[2]/span/div/select'
        self.__volume_place = '//*[@id="asset-data"]/div[2]/div/div[1]/div[5]/div/div/input'

        self.__price_place = '//*[@id="asset-data"]/div[2]/div/div[1]/div[6]/div/div/input'
        self.__comment_place = '//*[@id="general"]/div[4]/div/textarea'

        self.__price_request_place= '//*[@id="general"]/div[3]/div[1]/div/div[1]/span/label/span'
        self.__label_place= '//*[@id="general"]/div[2]/div[1]/div/div[1]/span/label/span'
        self.__proxy_place='//*[@id="asset-data"]/div[2]/div/div[1]/div[8]/div/div/div[2]/input'
        # new proxy place is //*[@id="asset-data"]/div[2]/div/div[8]/div/div/div[2]/input
        self.__registry_place= '//*[@id="general"]/div[3]/div[2]/div/div/div[2]/input'
        self.__counterparty_place = '//*[@id="general"]/div[1]/div[2]/div/div/div[2]/input'
        self.comment = 'Broker Indic:'
        self.counterparty = 'broker'


        # We have not really incorporated counterparties yet... how do we do that... 
    def __get_new_bidoffer(self):
        self.__driver.get('https://bid-offer-us.datahive.online/#/us_voluntary_us/form')
        #wait for page to load instead
        #WebDriverWait(self.__driver, 100)
        self.__wait_BO_page()
        
    def __login(self):
        self.__driver.get('https://bid-offer-us.datahive.online/#/login')
#         keyring.set_password('datahive', os.getlogin(), credentials.at[0,'pw'])
#         __password = keyring.get_password('datahive', os.getlogin())
#         __username = os.getlogin()
#         self.__write_value('input', 'data-cy', 'input-username', __username)
#         self.__write_value('input', 'data-cy', 'input-password', __password)
#         self.__get_value('button', 'data-cy', 'sign-in-button')
        self.__wait_main_page()
    
    def __get_value(self, type, type_of_text, text_to_find):
        element = self.__driver.find_element_by_xpath('//' + type + '[@' + type_of_text + '="' + text_to_find + '"]')
        self.__driver.execute_script("arguments[0].click();", element)

    def __write_value(self, type, type_of_text, text_to_find, mytext):
        element = self.__driver.find_element_by_xpath('//' + type + '[@' + type_of_text + '="' + text_to_find + '"]')
        element.send_keys(mytext)

    def __get_value_by_xpath(self, xpath):
        element = self.__driver.find_element_by_xpath(xpath)
        self.__driver.execute_script("arguments[0].click();", element)

    def __write_value_by_xpath(self, xpath, mytext):
        element = self.__driver.find_element_by_xpath(xpath)
        element.send_keys(mytext)

    def __get_value_by_xpath_press_enter(self, xpath):
        element = self.__driver.find_element_by_xpath(xpath)
        element.click()
        WebDriverWait(self.__driver, 20)
        element.send_keys(Keys.ENTER)
        
    def __get_value_by_xpath_press_enter_counterparty(self, xpath):
        element = self.__driver.find_element_by_xpath(xpath)
        element.click()
        WebDriverWait(self.__driver, 20)
        #Select(element).select_by_visible_text('Broker')
        #element.send_keys(Keys.ENTER)
        # UNDER CONSTRUCTION... -->
        option = self.__driver.find_element_by_xpath('/html/body/div[1]/div[1]/form/div/div/div[1]/div[1]/div[1]/div[2]/div/div/div[3]/ul/li[4]/span')
        option.click()
        option.send_keys(Keys.DOWN)
        option.send_keys(Keys.ENTER)
        # UNDER CONSTRUCTION... ^

    def __wait_main_page(self):
#         oldRoute = '/html/body/div[1]/div[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div/button[2]/span'
        
        
        WebDriverWait(self.__driver, 600).until(EC.visibility_of_element_located(
            (By.XPATH,'/html/body/div/div/div[1]/div[2]/div[1]/span[1]/label/span')))

    def __wait_BO_page(self):
        WebDriverWait(self.__driver, 600).until(EC.visibility_of_element_located((By.XPATH, self.__show_more_countries)))

        
    # Great... lets run through the code now... get rid of extraneous print statements... unnecessary comments etc. 
    def input_quote(self, quote): 
        
        
        # I added some reasoning here so that two separate bid offer forms are created when both bidPrice and OfferPrice are nonZero0
        self.__get_new_bidoffer()
        self.__driver.refresh()
        self.__get_new_bidoffer()
        
        
        
        # This is just to click the buttons for origin, technology and labels... 
        self.__get_value_by_xpath(self.__show_more_countries)
        self.__get_value_by_xpath(self.__show_more_labels)
        self.__get_value_by_xpath(self.__show_more_tech)
        
        # PRICE
        try:
            self.__write_value_by_xpath(self.__price_place, quote.price)
            print("Bid price is:" + str(quote.bidPrice))
            print("Offer price is:" + str(quote.offerPrice))
        except:
            pass

        # REQUEST?
#        if quote.price_request:
#            try:
#                self.__get_value_by_xpath(self.__price_request_place)
#            except:
#                pass
        #PROXY
    
    
        try:#***********FIX THIS... 
            print("The proxy is: " + str(quote.proxy))
            self.__get_value_by_xpath(self.__proxy_place)
            self.__write_value_by_xpath(self.__proxy_place, str(quote.proxy))
            self.__get_value_by_xpath_press_enter(self.__proxy_place)
        except:
            pass
        #COMMENT
        try:
            self.__write_value_by_xpath(self.__comment_place, self.comment + quote.raw)
        except: 
            pass
        # YEAR
        try:
            self.__write_value_by_xpath(self.__year_start_place, quote.start_year)
            self.__write_value_by_xpath(self.__year_end_place, quote.end_year)

        except:
            pass

        # MONTHS
        try:
            self.__write_value_by_xpath(self.__month_start_place, quote.start_month)
            self.__write_value_by_xpath(self.__month_end_place, quote.end_month)
        except:
            pass

        # BROKER
        try:
            self.__get_value_by_xpath(self.__broker_place)
            self.__write_value_by_xpath(self.__broker_place, quote.broker)
            print("Broker: " + str(quote.broker))
            #self.__get_value_by_xpath_press_down(self.__counterparty_place, 4)
            self.__get_value_by_xpath_press_enter(self.__broker_place)
        except:
            pass

        # TYPE
        try:
            self.__get_value('input', 'value', quote.quote_type)
        except:
            pass

        #"//*[@id='origins']/div[2]/div/div//span[@title='Yukon']"

        # ORIGIN.
        try: #Note: I found relative xpaths from Chrome to get value etc. by xpath
            print("origin is")
            print(quote.origin)

            #self.__get_value_by_xpath("//*[@id='origins']/div[2]/div/div/span/span/label/span[@title='"+str(quote.origin)+"']")

            for curr_origin in quote.origin:
                self.__get_value_by_xpath("//*[@id='origins']/div[2]/div/div/span/span/label/span[@title='" + str(curr_origin) + "']")

        except:
            pass

#         def __get_value(self, type, type_of_text, text_to_find)
#         element = self.__driver.find_element_by_xpath('//' + type + '[@' + type_of_text + '="' + text_to_find + '"]')
#         self.__driver.execute_script("arguments[0].click();", element)

        # SUPPORT
        try:
            self.__get_value('input', 'value', quote.support_type)
        except:
            pass
        
        try:
            for curr_tech in quote.tech:
                print("Tech: "+str(curr_tech))
                
                self.__get_value_by_xpath("//*[@id='products']/div/div/div/span/span/label/span[@title='"+str(curr_tech)+"']")
        except:
            pass

        # VOLUME
        try: 
            print("Volume: " + str(quote.volume))
            self.__write_value_by_xpath(self.__volume_place, quote.volume)
        except:
            pass

        # LABEL
        try:
            print(quote.label)
            for curr_label in quote.label:
                print("Label: " + str(curr_label))
                #//*[@id="labels"]/div[2]/div//span[@title='Ares Non-Wind']
     
                print(str(curr_label))
                #[@title = 'CRS Listed']
                self.__get_value_by_xpath("//*[@id='labels']/div[2]/div/span/span/label/span[@title='"+str(curr_label)+"']")
        except:
            pass

        # COUNTERPARTY
        try:
            # I see what the problem is... I have not yet given a counterparty attributes to the quote... so when we call on quote.counterparty... it just writes a blank into the damned thing... okay...lets go set it and see what happens... 
            self.__get_value_by_xpath(self.__counterparty_place)
            self.__write_value_by_xpath(self.__counterparty_place, quote.counterparty)
            
            # This is the part where I want to make the change... The enter function by itself is not doing anything... 
            # We need slightly more granular code here... 
            # How do we do this... We need to first click on the counterparty box... then we need to input the name broker... 
            # Then we need to click on the Broker thing in the drop down... currently it is not accessing the dropdown... it is just 
            # stupidly pressing enter... I want to fix this... 
            
            self.__get_value_by_xpath_press_enter(self.__counterparty_place)
            # self.__get_value_by_xpath('//*[@id="general"]/div[1]/div[2]/div/div/div[3]/ul/li[4]/span/div/span')
            
        except:
            pass


        # REGISTRY
        try:
            print("Registry: " + str(quote.registry))
            self.__get_value_by_xpath(self.__registry_place)
            self.__write_value_by_xpath(self.__registry_place, quote.registry)
            self.__get_value_by_xpath_press_enter(self.__registry_place)
        except:
            pass
        
        pass 
        #self.__wait_main_page()
        
    # I wrote this method to allow us to open and access a new form (in a new tab) to add both bid and offer prices simultaneously... 
    def switchWindow(self):
        self.__driver.execute_script("window.open('');")
        self.__driver.switch_to.window(self.__driver.window_handles[len(self.__driver.window_handles)-1])
        self.__driver.get('https://bid-offer-us.datahive.online/#/us_voluntary_us/form')
        
    def closeWindow(self):
        self.__driver.close()
        self.__driver.switch_to.window(self.__driver.window_handles[len(self.__driver.window_handles)-1])
        
    def input_quote_bidOffer(self, quote, typeIsBid): # This is exactly the same except you have to specify if you have a bidPrice or not
        # When you do, it fills in a form with the bid price... otherwise it fills it in with offerPrice... in the former case the type is
        # bid... in the latter case it is offer... 
        
        # The code beyond this is not working... it is not spinning up a bid offer wala form... why? 
        # we have all the information needed to fill the form... if it loaded up a form then we have what we need to fill it... 
        # the problem is that it is not loading up the form... why?
        self.__get_new_bidoffer()
        self.__driver.refresh()
        self.__get_new_bidoffer()
        
        # This is just to click the buttons for origin, technology and labels... 
        self.__get_value_by_xpath(self.__show_more_countries)
        self.__get_value_by_xpath(self.__show_more_labels)
        self.__get_value_by_xpath(self.__show_more_tech)
        
        # PRICE
        try:
            if typeIsBid:
                self.__write_value_by_xpath(self.__price_place, quote.bidPrice)
            else:
                self.__write_value_by_xpath(self.__price_place, quote.offerPrice)
        except:
            pass

        # REQUEST?
#        if quote.price_request:
#            try:
#                self.__get_value_by_xpath(self.__price_request_place)
#            except:
#                pass

        #PROXY
        try:#***********FIX THIS... 
            print("Proxy:" + str(quote.proxy))
            self.__get_value_by_xpath(self.__proxy_place)
            self.__write_value_by_xpath(self.__proxy_place, str(quote.proxy))
            self.__get_value_by_xpath_press_enter(self.__proxy_place)
        except:
            pass

        #COMMENT
        try:
            self.__write_value_by_xpath(self.__comment_place, self.comment + quote.raw)
        except: 
            pass
        # YEAR
        try:
            self.__write_value_by_xpath(self.__year_start_place, quote.start_year)
            self.__write_value_by_xpath(self.__year_end_place, quote.end_year)

        except:
            pass

        # MONTHS
        try:
            self.__write_value_by_xpath(self.__month_start_place, quote.start_month)
            self.__write_value_by_xpath(self.__month_end_place, quote.end_month)
        except:
            pass

        # BROKER
        try:
            self.__get_value_by_xpath(self.__broker_place)
            self.__write_value_by_xpath(self.__broker_place, quote.broker)
            #error press enter
            self.__get_value_by_xpath_press_enter(self.__broker_place)
        except:
            pass

        # TYPE
        try:
            if typeIsBid:
                self.__get_value('input', 'value', 'bid')
            else:
                self.__get_value('input', 'value', 'offer')
        except:
            pass
        
        #"//*[@id='origins']/div[2]/div/div//span[@title='Yukon']"

        # ORIGIN.
        try: #Note: I found relative xpaths from Chrome to get value etc. by xpath
            print("Origin: " + str(quote.origin))
            for curr_origin in quote.origin:
                self.__get_value_by_xpath("//*[@id='origins']/div[2]/div/div/span/span/label/span[@title='" + str(curr_origin) + "']")
            
        except:
            pass

#         def __get_value(self, type, type_of_text, text_to_find)
#         element = self.__driver.find_element_by_xpath('//' + type + '[@' + type_of_text + '="' + text_to_find + '"]')
#         self.__driver.execute_script("arguments[0].click();", element)
        

        # SUPPORT
        try:
            self.__get_value('input', 'value', quote.support_type)
        except:
            pass
        
        #TECH
        try:
            for curr_tech in quote.tech:
                print("Tech: " + str(curr_tech))
                # //*[@id="products"]/div[3]/div/div/span[12]/span/label/span
                self.__get_value_by_xpath("//*[@id='products']/div/div/div/span/span/label/span[@title='"+str(curr_tech)+"']")
        except:
            pass

        # VOLUME
        try: 
            print('volume: ' + str(quote.volume))
            self.__write_value_by_xpath(self.__volume_place, quote.volume)
        except:
            pass

        # LABEL
        try:
            for curr_label in quote.label:
                print("Label: " + str(curr_label))
                #//*[@id="labels"]/div[2]/div//span[@title='Ares Non-Wind']
                # //*[@id="labels"]/span/div/span[2]/span/label/span[@title='Ares Non-Wind']

                self.__get_value_by_xpath("//*[@id='labels']/div[2]/div/span/span/label/span[@title='"+str(curr_label)+"']")
        except:
            pass


        try: # COUNTERPARTY
            self.__get_value_by_xpath(self.__counterparty_place)
            self.__write_value_by_xpath(self.__counterparty_place, quote.counterparty)
            self.__get_value_by_xpath_press_enter(self.__counterparty_place)
            # There needs to be a change  here... we need to write further code so that it goes into the dropdown and actually selects 
            # broker... lets see how proxy product is done.... it might give us clues... 
            
            # I think that a good strategy might be to select by visible text... the visible text should be broker... and if we select by 
            # that then it should work just fine... 
            
            
            # Is the volume and the label etc. being dealt with multiple times? Why... we need to change this... 
            
        except:
            pass


        # REGISTRY
        try:
            print("Registry: " + str(quote.registry))
            self.__get_value_by_xpath(self.__registry_place)
            self.__write_value_by_xpath(self.__registry_place, quote.registry)
            self.__get_value_by_xpath_press_enter(self.__registry_place)
        except:
            pass
        
        pass 
        #self.__wait_main_page()