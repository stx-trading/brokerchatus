import pandas as pd

from webdriver import Webdriver
import re


class Textreader:
    driver: Webdriver

    def __init__(self, mytext, driver):
        self.mytext = mytext.replace("'", "").replace("\n", ' ').replace('\r', '').replace(',', '.')
        self.mytext_nomaj = self.mytext.lower()
        self.driver = driver

        bid_offers_input = ['BO', 'request Type', 'CP', 'Origin', 'Prod start year', 'Prod start month',
                            'Prod end year', 'Prod end month', 'COD', 'Volume', 'Price', 'Label', 'Tech', 'Support']

        self.broker_names = ['aksel', 'dvasch']
        self.trading_team = ['Lorenzo', 'Daan', 'Klemen', 'Lu', 'Michel', 'Rishi']

        self.bid_offers = pd.DataFrame(columns=bid_offers_input)

        self.__create_bidoffer()

    def __text_splitter(self):
        # first we split with broker name
        self.list_of_quotes = re.split('[0-9][0-9]:[0-9][0-9]:[0-9][0-9]', self.mytext)
        #  we delete our chats
        self.list_of_quotes = [quote for quote in self.list_of_quotes if
                               not any([trader in quote for trader in self.trading_team])]
        # we clean the quotes
        for quote in self.list_of_quotes:
            if 'trades' in self.list_of_quotes:
                self.list_of_quotes.remove(quote)
            if (not self.__is_request(quote)) or (not self.__is_quote(quote)):
                self.list_of_quotes.remove(quote)
            # if there is no year in it
            number_of_years = len([i for i in self.years if i in quote])
            if number_of_years < 1:
                self.list_of_quotes.remove(quote)

        for quote in self.list_of_quotes:
            # we check if there is multiple years in the quote
            number_of_years = len([i for i in self.years if i in quote])
            if number_of_years > 1:
                new_quote = re.split('[0-9][0-9][0-9][0-9](.*)|-[0-9][0-9](.*)', quote)
                self.list_of_quotes.remove(quote)
                self.list_of_quotes.append(new_quote)

        # we split if there is a bid and a offer in the quote
        for quote in self.list_of_quotes:
            if len(re.findall(r'[0-9][0-9].[0-9][0-9]-[0-9][0-9].[0-9][0-9]')) > 0:
        # we first find the year of the quote

    def __is_request(self, quote):
        if 'request' in quote:
            return True
        if 'looking for' in quote:
            return True

        return False

    def __is_quote(self, quote):
        check_if_price = len(re.findall(r'[0-9].[0-9][0-9]')) > 0
        check_if_type = any([word in quote for word in ['bid', 'offer']])
        check_if_BO = len(re.findall(r'[0-9].[0-9][0-9]-[0-9].[0-9][0-9]|[0-9].[0-9][0-9] - [0-9].[0-9][0-9]')) > 0

        if (check_if_price and check_if_type) or check_if_BO:
            return True
        else:
            return False

    def __create_bidoffer(self):
        self.driver.get_new_bidoffer()
        self.__enter_inputs()

        # self.__get_currency()
        # self.__show()

    def __show(self):
        show_more_countries = '// *[ @ id = "origins"] / div[2] / div / button / div / span[1]'
        show_more_labels = '//*[@id="labels"]/div[2]/button/div'
        self.driver.get_value_by_xpath(show_more_countries)
        self.driver.get_value_by_xpath(show_more_labels)

    def __get_bidoffer(self):
        if 'bid' in self.mytext:
            self.driver.get_value('input', 'value', 'bid')
        elif 'offer' in self.mytext:
            self.driver.get_value('input', 'value', 'offer')

    def __get_support(self):
        if ('no support' in self.mytext) | ('unsup' in self.mytext):
            self.driver.get_value('input', 'value', 'unsupported')

    def __get_country(self):
        for key, value in self.countries[self.broker_dict].items():
            if key in self.mytext:
                self.driver.get_value('span', 'title', self.countries[self.broker_dict][key])
                break

    def __get_year(self):
        for key, value in self.years.items():
            if key in self.mytext:
                self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[1]/div/div[1]/div/div/select',
                                                 self.years[key])
                break

    def __get_months(self):
        count = 0
        is_quarter = any([key in self.mytext for key, value in self.quarters.items()])
        is_semester = any([key in self.mytext for key, value in self.semesters.items()])

        if is_quarter:
            for key, value in self.quarters.items():
                if key in self.mytext:
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[1]/div/div[2]/div/div/select',
                                                     self.quarters[key][0])
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[2]/div/div[2]/div/div/select',
                                                     self.quarters[key][1])
                    return

        if is_semester:
            for key, value in self.semesters.items():
                if key in self.mytext:
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[1]/div/div[2]/div/div/select',
                                                     self.semesters[key][0])
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[2]/div/div[2]/div/div/select',
                                                     self.semesters[key][1])
                    return

        for key, value in self.months.items():
            if key in self.mytext_nomaj:
                count += 1
        if count == 1:
            for key, value in self.months.items():
                if key in self.mytext_nomaj:
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[1]/div/div[2]/div/div/select',
                                                     self.months[key])
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[2]/div/div[2]/div/div/select',
                                                     self.months[key])
                    break
        if count == 2:
            temp_count = 0
            for key, value in self.months.items():
                if key in self.mytext_nomaj:
                    temp_count += 1
                    self.driver.write_value_by_xpath(
                        '//*[@id="asset-data"]/div[2]/div[' + str(temp_count) + ']/div/div[2]/div/div/select',
                        self.months[key])
                    if temp_count == 2:
                        break

    def __get_tech(self):
        for key, value in self.tech[self.broker_dict].items():
            if key in self.mytext_nomaj:
                self.driver.get_value('span', 'title', self.tech[self.broker_dict][key])
                break

    def __get_volume(self):
        no_volume = True
        if 'gw' in self.mytext_nomaj:
            for num in map(int, [num.replace('-', '').replace(' ', '') for num in
                                 re.findall(r'\s\d+\s|\-\d+', self.mytext_nomaj)]):
                if (num not in self.years_list) & (isinstance(num, int)):
                    no_volume = False
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[6]/div/div/input',
                                                     str(num * 1000))
                    break

        if 'mw' in self.mytext_nomaj:
            numbers = map(int, re.findall(r'\d+', self.mytext_nomaj))
            for num in numbers:
                if (num not in self.years_list) & (isinstance(num, int)):
                    no_volume = False
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[6]/div/div/input', str(num))
                    break
        if no_volume:
            self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[6]/div/div/input', str(50000))

    def __get_price(self):
        self.price_request = not (any([isinstance(num, float) for num in map(float, [w.replace(',', '.') for w in
                                                                                     re.findall(r"\d+\,\d+",
                                                                                                self.mytext_nomaj)])]) |
                                  any([isinstance(num, float) for num in map(float, [w.replace(',', '.') for w in
                                                                                     re.findall(r"\d+\.\d+",
                                                                                                self.mytext_nomaj)])])) or 'request' in self.mytext_nomaj

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\,\d+", self.mytext_nomaj)]):
            if (num not in self.years_list) & (isinstance(num, float)) & (num < 5):
                self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[7]/div/div/input', str(num))
                break

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\.\d+", self.mytext_nomaj)]):
            if (num not in self.years_list) & (isinstance(num, float)) & (num < 5):
                self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[7]/div/div/input', str(num))
                break

    def __get_price_request(self):
        if self.price_request:
            self.driver.get_value_by_xpath('//*[@id="general"]/div[2]/div[1]/div/div[1]/label/span')

    def __get_label(self):
        for key, value in self.label[self.broker_dict].items():
            if key in self.mytext_nomaj:
                self.driver.get_value('span', 'title', self.label[self.broker_dict][key])
                break
