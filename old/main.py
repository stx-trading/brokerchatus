from webdriver import Webdriver
from textreader import Textreader

def main():
    webdriver = Webdriver()

    while True:
        print('   ')
        print('Enter your quote')
        lines = []
        while True:
            line = input()
            if line:
                lines.append(line)
            else:
                break
        my_input = '\n'.join(lines)
        Textreader(mytext= my_input, driver=webdriver)

if __name__ == '__main__':
    main()