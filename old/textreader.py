from webdriver import Webdriver
import re


class Textreader:
    driver: Webdriver

    def __init__(self, mytext, driver):
        self.mytext = mytext.replace("'", "").replace("\n", ' ').replace('\r', '')
        self.mytext_nomaj = self.mytext.lower()
        self.driver = driver
        self.countries = {
            'ICAP': {'Scandi': 'Scandinavia', 'Nordic': 'Scandinavia', 'conn': 'AIB grid-connected',
                     'no island': 'Europe AIB ex Iceland', 'IT': 'Italy',
                     'Norway': 'Norway', 'orw': 'Norway', 'NO': 'Norway', 'FR': 'France', 'EU': 'EU AIB',
                     'AIB': 'Europe AIB',
                     'NL': 'Netherlands', 'DE': 'Germany', 'Germany': 'Germany', 'FI': 'Finland', 'ES': 'Spain',
                     'DK': 'Denmark','SE':'Sweden', 'Swedish': 'Sweden', 'swed': 'Sweden', 'Swed': 'Sweden', 'French': 'France',
                     'Alpine': 'Alpine',
                     'alpine': 'alpine'},
            'Commerg': {'Nordic': 'Scandinavia', 'conn': 'AIB grid-connected', 'IT': 'Italy',
                        'Norway': 'Norway', 'NO': 'Norway', 'FR': 'France', 'EU': 'EU AIB', 'AIB': 'Europe AIB',
                        'NL': 'Netherlands', 'DE': 'Germany', 'Germany': 'Germany', 'FI': 'Finland', 'ES': 'Spain',
                        'DK': 'Denmark', 'Swedish': 'Sweden', 'Alpine': 'Alpine', 'alpine': 'alpine',
                        'cesar/eecs': 'Cesar / EECS', 'cesar': 'Cesar'}}
        self.years = {'2020': '2020', '2021': '2021', '2022': '2022', '2023': '2023', '2024': '2024', '2025': '2025',
                      '2026': '2026', '2027': '2027', '2028': '2028', '2029': '2029', '-20': '2020', '-1': '2021',
                      '-22': '2022', '-23': '2023', '-24': '2024', '-25': '2025', '-26': '2026', '-27': '2027',
                      '-28': '2028', '-29': '2029'}

        self.months = {'jan': 'Jan', 'feb': 'Feb', 'mar': 'Mar', 'apr': 'Apr', 'may': 'May', 'jun': 'Jun', 'jul': 'Jul',
                       'aug': 'Aug', 'sep': 'Sep', 'oct': 'Oct', 'nov': 'Nov', 'dec': 'Dec'}

        self.quarters = {'Q1': ['Jan', 'Mar'], 'Q2': ['Apr', 'Jun'], 'Q3': ['Jul', 'Sep'], 'Q4': ['Oct', 'Dec']}

        self.semesters = {'H1': ['Jan', 'Jun'], 'H2': ['Jul', 'Dec']}

        self.years_list = [2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2027, 2028, 2029, 2030, 20, 21, 22, 23, 24,
                           25, 26, 27, 28, 29, 30]
        self.tech = {
            'ICAP': {'any res': 'Renewables', 'res': 'Renewables', 'hydro': 'Hydro', 'wind': 'Wind', 'solar': 'Solar', 'bio': 'Biomass'}}

        self.label = {
            'ICAP': {'cfd': 'CFD', 'proof': 'Proof of Powerflow', 'expl': 'Explicit Flows', 'impl': 'Implicit Flows',
                     'milj': 'Bra Miljöval', 'domestic': 'Spanish Domestic', 'tuv': 'TÜV SÜD EE', 'tüv': 'TÜV SÜD EE'},
            'Commerg': {'cfd': 'CFD', 'proof': 'Proof of Powerflow', 'expl': 'Explicit Flows', 'impl': 'Implicit Flows',
                        'milj': 'Bra Miljöval', 'domestic': 'Spanish Domestic', 'tuv': 'TÜV SÜD EE',
                        'tüv': 'TÜV SÜD EE'}}

        self.__create_bidoffer()

    def __create_bidoffer(self):
        self.driver.get_new_bidoffer()
        self.__show()
        self.__get_price()
        self.__get_price_request()
        self.__get_year()
        self.__get_months()
        self.__get_broker()
        self.__get_bidoffer()
        self.__get_country()
        self.__get_support()
        self.__get_tech()
        self.__get_volume()
        self.__get_label()

        # self.__get_currency()
        # self.__show()

    def __show(self):
        show_more_countries = '// *[ @ id = "origins"] / div[2] / div / button / div / span[1]'
        show_more_labels = '//*[@id="labels"]/div[2]/button/div'
        self.driver.get_value_by_xpath(show_more_countries)
        self.driver.get_value_by_xpath(show_more_labels)

    def __get_bidoffer(self):
        if 'bid' in self.mytext:
            self.driver.get_value('input', 'value', 'bid')
        elif 'offer' in self.mytext:
            self.driver.get_value('input', 'value', 'offer')

    def __get_broker(self):
        self.broker_dict = 'ICAP'
        self.broker = 'TP ICAP (Europe) S.A.'
        if 'dvasch' in self.mytext:
            self.broker = 'Commerg Ltd'
            self.broker_dict = 'Commerg'
        elif 'aksel' in self.mytext:
            self.broker = 'TP ICAP (Europe) S.A.'
            self.broker_dict = 'ICAP'

        self.driver.get_value_by_xpath('//*[@id="general"]/div[1]/div[2]/div/div/div[2]/input')
        self.driver.write_value_by_xpath('//*[@id="general"]/div[1]/div[2]/div/div/div[2]/input', self.broker)
        self.driver.get_value_by_xpath_press_enter('//*[@id="general"]/div[1]/div[2]/div/div/div[2]/input')

    def __get_support(self):
        if ('no support' in self.mytext) | ('unsup' in self.mytext):
            self.driver.get_value('input', 'value', 'unsupported')

    def __get_country(self):
        for key, value in self.countries[self.broker_dict].items():
            if key in self.mytext:
                self.driver.get_value('span', 'title', self.countries[self.broker_dict][key])
                break

    def __get_year(self):
        for key, value in self.years.items():
            if key in self.mytext:
                self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[1]/div/div[1]/div/div/select',
                                                 self.years[key])
                break

    def __get_months(self):
        count = 0
        is_quarter = any([key in self.mytext for key, value in self.quarters.items()])
        is_semester = any([key in self.mytext for key, value in self.semesters.items()])

        if is_quarter:
            for key, value in self.quarters.items():
                if key in self.mytext:
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[1]/div/div[2]/div/div/select',
                                                     self.quarters[key][0])
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[2]/div/div[2]/div/div/select',
                                                     self.quarters[key][1])
                    return

        if is_semester:
            for key, value in self.semesters.items():
                if key in self.mytext:
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[1]/div/div[2]/div/div/select',
                                                     self.semesters[key][0])
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[2]/div/div[2]/div/div/select',
                                                     self.semesters[key][1])
                    return

        for key, value in self.months.items():
            if key in self.mytext_nomaj:
                count += 1
        if count == 1:
            for key, value in self.months.items():
                if key in self.mytext_nomaj:
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[1]/div/div[2]/div/div/select',
                                                     self.months[key])
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[2]/div/div[2]/div/div/select',
                                                     self.months[key])
                    break
        if count == 2:
            temp_count = 0
            for key, value in self.months.items():
                if key in self.mytext_nomaj:
                    temp_count += 1
                    self.driver.write_value_by_xpath(
                        '//*[@id="asset-data"]/div[2]/div[' + str(temp_count) + ']/div/div[2]/div/div/select',
                        self.months[key])
                    if temp_count == 2:
                        break

    def __get_tech(self):
        for key, value in self.tech[self.broker_dict].items():
            if key in self.mytext_nomaj:
                self.driver.get_value('span', 'title', self.tech[self.broker_dict][key])
                break

    def __get_volume(self):
        no_volume = True
        if 'gw' in self.mytext_nomaj:
            for num in map(int, [num.replace('-', '').replace(' ', '') for num in
                                 re.findall(r'\s\d+\s|\-\d+', self.mytext_nomaj)]):
                if (num not in self.years_list) & (isinstance(num, int)):
                    no_volume = False
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[6]/div/div/input',
                                                     str(num * 1000))
                    break

        if 'mw' in self.mytext_nomaj:
            numbers = map(int, re.findall(r'\d+', self.mytext_nomaj))
            for num in numbers:
                if (num not in self.years_list) & (isinstance(num, int)):
                    no_volume = False
                    self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[6]/div/div/input', str(num))
                    break
        if no_volume:
            self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[6]/div/div/input', str(50000))

    def __get_price(self):
        self.price_request = not (any([isinstance(num, float) for num in map(float, [w.replace(',', '.') for w in
                                                                                     re.findall(r"\d+\,\d+",
                                                                                                self.mytext_nomaj)])]) |
                                  any([isinstance(num, float) for num in map(float, [w.replace(',', '.') for w in
                                                                                     re.findall(r"\d+\.\d+",
                                                                                                self.mytext_nomaj)])])) or 'request' in self.mytext_nomaj

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\,\d+", self.mytext_nomaj)]):
            if (num not in self.years_list) & (isinstance(num, float)) & (num < 5):
                self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[7]/div/div/input', str(num))
                break

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\.\d+", self.mytext_nomaj)]):
            if (num not in self.years_list) & (isinstance(num, float)) & (num < 5):
                self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[7]/div/div/input', str(num))
                break

    def __get_price_request(self):
        if self.price_request:
            self.driver.get_value_by_xpath('//*[@id="general"]/div[2]/div[1]/div/div[1]/label/span')

    def __get_currency(self):
        self.driver.write_value_by_xpath('//*[@id="asset-data"]/div[2]/div[8]/div/div/select', 'EUR')

    def __get_label(self):
        for key, value in self.label[self.broker_dict].items():
            if key in self.mytext_nomaj:
                self.driver.get_value('span', 'title', self.label[self.broker_dict][key])
                break
