from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from time import sleep
import keyring
import os

class Webdriver:
    def __init__(self):
        options = Options()
        if os.getlogin() == 'mleveque':
            options.binary_location = r'C:\Users\mleveque.old\AppData\Local\Mozilla Firefox\firefox.exe'
        elif os.getlogin() == 'kiskra':
            options.binary_location = 'C:\Program Files\Mozilla Firefox\firefox.exe'
        driver_path = r'G:\STX v1\2 Desks\2.3 Green\2.3.27 Trading Desk\30. MV\Broker Chat\geckodriver.exe'
        self.driver = webdriver.Firefox(executable_path=driver_path, options=options)
        self.driver.maximize_window()
        sleep(3)
        self.__login()

        self.show_more_countries = '// *[ @ id = "origins"] / div[2] / div / button / div / span[1]'
        self.show_more_labels = '//*[@id="labels"]/div[2]/button/div'
        self.broker_place = '//*[@id="general"]/div[1]/div[2]/div/div/div[2]/input'
        self.year_place = '//*[@id="asset-data"]/div[2]/div[1]/div/div[1]/div/div/select'
        self.month_start_place = '//*[@id="asset-data"]/div[2]/div[1]/div/div[2]/div/div/select'
        self.month_end_place = '//*[@id="asset-data"]/div[2]/div[2]/div/div[2]/div/div/select'
        self.volume_place = '//*[@id="asset-data"]/div[2]/div[6]/div/div/input'
        self.price_place = '//*[@id="asset-data"]/div[2]/div[7]/div/div/input'
        self.price_request_place = '//*[@id="general"]/div[2]/div[1]/div/div[1]/label/span'
        self.label_place = '//*[@id="general"]/div[2]/div[1]/div/div[1]/label/span'


    def __get_new_bidoffer(self):
        self.driver.get('https://bid-offer.datahive.online/#/goo/form')
        sleep(1)

    def __login(self):
        self.driver.get('https://bid-offer.datahive.online/#/login')
        sleep(3)
        __password = keyring.get_password('datahive', 'mleveque')
        __username = os.getlogin()
        self.__write_value('input', 'data-cy', 'input-username', __username)
        self.__write_value('input', 'data-cy', 'input-password', __password)
        self.__get_value('button', 'data-cy', 'sign-in-button')

    def __get_value(self, type, type_of_text, text_to_find):
        element = self.driver.find_element_by_xpath('//' + type + '[@' + type_of_text + '="' + text_to_find + '"]')
        self.driver.execute_script("arguments[0].click();", element)

    def __write_value(self, type, type_of_text, text_to_find, mytext):
        element = self.driver.find_element_by_xpath('//' + type + '[@' + type_of_text + '="' + text_to_find + '"]')
        element.send_keys(mytext)

    def __get_value_by_xpath(self, xpath):
        element = self.driver.find_element_by_xpath(xpath)
        self.driver.execute_script("arguments[0].click();", element)

    def __write_value_by_xpath(self, xpath, mytext):
        element = self.driver.find_element_by_xpath(xpath)
        element.send_keys(mytext)

    def __get_value_by_xpath_press_enter(self, xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.send_keys(Keys.RETURN)

    def input_quote(self, quote):
        # to show all countries and labels
        self.__get_value_by_xpath(self.show_more_countries)
        self.__get_value_by_xpath(self.show_more_labels)

        # PRICE
        try:
            self.__get_value_by_xpath(self.price_place, quote.price)
        except:
            pass

        # REQUEST?
        if quote.price_request:
            try:
                self.__get_value_by_xpath(self.price_request_place)
            except:
                pass

        # YEAR
        try:
            self.__write_value_by_xpath(self.year_place, quote.year)
        except:
            pass

        # MONTHS
        try:
            self.__write_value_by_xpath(self.month_start_place, quote.start_month)
            self.__write_value_by_xpath(self.month_end_place, quote.end_month)
        except:
            pass

        # BROKER
        try:
            self.__get_value_by_xpath(self.broker_place)
            self.__write_value_by_xpath(self.broker_place, quote.broker)
            self.__get_value_by_xpath_press_enter(self.broker_place)
        except:
            pass

        # TYPE
        try:
            self.__get_value('input', 'value', quote.quote_type)
        except:
            pass

        # COUNTRY
        try:
            self.__get_value('span', 'title', quote.country)
        except:
            pass

        # SUPPORT
        try:
            self.__get_value('input', 'value', quote.support_type)
        except:
            pass

        # TECH
        try:
            self.__get_value('span', 'title', quote.tech)
        except:
            pass

        # VOLUME
        try:
            self.__write_value_by_xpath(self.volume_place, quote.volume)
        except:
            pass

        # LABEL
        try:
            self.__get_value('span', 'title', self.label_place)
        except:
            pass

