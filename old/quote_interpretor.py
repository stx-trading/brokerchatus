import re


class Quote_Interpretor:
    def __init__(self, quote):
        self.quote = quote
        self.quote_no_maj = quote.lower()

        self.countries_voc = {
            'ICAP': {'Scandi': 'Scandinavia', 'Nordic': 'Scandinavia', 'conn': 'AIB grid-connected', 'IT': 'Italy',
                     'Norway': 'Norway', 'NO': 'Norway', 'FR': 'France', 'EU': 'EU AIB', 'AIB': 'Europe AIB',
                     'NL': 'Netherlands', 'DE': 'Germany', 'Germany': 'Germany', 'FI': 'Finland', 'ES': 'Spain',
                     'DK': 'Denmark', 'Swedish': 'Sweden', 'Alpine': 'Alpine', 'alpine': 'alpine'},
            'Commerg': {'Nordic': 'Scandinavia', 'conn': 'AIB grid-connected', 'IT': 'Italy',
                        'Norway': 'Norway', 'NO': 'Norway', 'FR': 'France', 'EU': 'EU AIB', 'AIB': 'Europe AIB',
                        'NL': 'Netherlands', 'DE': 'Germany', 'Germany': 'Germany', 'FI': 'Finland', 'ES': 'Spain',
                        'DK': 'Denmark', 'Swedish': 'Sweden', 'Alpine': 'Alpine', 'alpine': 'alpine'}}

        self.years_voc = {'2020': '2020', '2021': '2021', '2022': '2022', '2023': '2023', '2024': '2024',
                          '2025': '2025',
                          '2026': '2026', '2027': '2027', '2028': '2028', '2029': '2029', '-20': '2020', '-21': '2021',
                          '-22': '2022', '-23': '2023', '-24': '2024', '-25': '2025', '-26': '2026', '-27': '2027',
                          '-28': '2028', '-29': '2029'}

        self.months_voc = {'jan': 'Jan', 'feb': 'Feb', 'mar': 'Mar', 'apr': 'Apr', 'may': 'May', 'jun': 'Jun',
                           'jul': 'Jul',
                           'aug': 'Aug', 'sep': 'Sep', 'oct': 'Oct', 'nov': 'Nov', 'dec': 'Dec'}

        self.quarters_voc = {'Q1': ['Jan', 'Mar'], 'Q2': ['Apr', 'Jun'], 'Q3': ['Jul', 'Sep'], 'Q4': ['Oct', 'Dec']}

        self.semesters_voc = {'H1': ['Jan', 'Jun'], 'H2': ['Jul', 'Dec']}

        self.years_voc = [2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2027, 2028, 2029, 2030, 20, 21, 22, 23, 24,
                          25, 26, 27, 28, 29, 30]
        self.tech_voc = {
            'ICAP': {'any res': 'Renewables', 'res': 'Renewables', 'hydro': 'Hydro', 'wind': 'Wind', 'solar': 'Solar'}}

        self.label_voc = {
            'ICAP': {'cfd': 'CFD', 'proof': 'Proof of Powerflow', 'expl': 'Explicit Flows', 'impl': 'Implicit Flows',
                     'milj': 'Bra Miljöval', 'domestic': 'Spanish Domestic', 'tuv': 'TÜV SÜD EE', 'tüv': 'TÜV SÜD EE'},
            'Commerg': {'cfd': 'CFD', 'proof': 'Proof of Powerflow', 'expl': 'Explicit Flows', 'impl': 'Implicit Flows',
                        'milj': 'Bra Miljöval', 'domestic': 'Spanish Domestic', 'tuv': 'TÜV SÜD EE',
                        'tüv': 'TÜV SÜD EE'}}

    def interprete(self):
        self.__get_price()
        self.__get_year()
        self.__get_months()
        self.__get_broker()
        self.__get_bidoffer()
        self.__get_country()
        self.__get_support()
        self.__get_tech()
        self.__get_volume()
        self.__get_label()

    def __get_bidoffer(self):
        if 'offer' in self.quote:
            self.quote_type = 'offer'
        if 'bid' in self.quote:
            self.quote_type = 'bid'
        if 'looking for' in self.quote and 'offer' in self.quote:
            self.quote_type = 'bid'
        if 'looking for' in self.quote and 'bid' in self.quote:
            self.quote_type = 'offer'

    def __get_broker(self):
        self.broker_dict = 'ICAP'
        self.broker = 'TP ICAP (Europe) S.A.'
        if 'dvasch' in self.quote:
            self.broker = 'Commerg Ltd'
            self.broker_dict = 'Commerg'
        elif 'aksel' in self.quote:
            self.broker = 'TP ICAP (Europe) S.A.'
            self.broker_dict = 'ICAP'

    def __get_support(self):
        if ('no support' in self.quote) | ('unsup' in self.quote):
            self.support_type = 'unsupported'
        else:
            self.support_type = 'supported'

    def __get_country(self):
        for key, value in self.countries_voc[self.broker_dict].items():
            if key in self.quote:
                self.country = self.countries_voc[self.broker_dict][key]
                break

    def __get_year(self):
        for key, value in self.years_voc.items():
            if key in self.quote:
                self.year = self.years_voc[key]
                break

    def __get_months(self):
        count = 0
        is_quarter = any([key in self.quote for key, value in self.quarters_voc.items()])
        is_semester = any([key in self.quote for key, value in self.semesters_voc.items()])

        if is_quarter:
            for key, value in self.quarters_voc.items():
                if key in self.quote:
                    self.start_month = self.quarters_voc[key][0]
                    self.end_month = self.quarters_voc[key][1]
                    return

        if is_semester:
            for key, value in self.semesters_voc.items():
                if key in self.quote:
                    self.start_month = self.semesters_voc[key][0]
                    self.end_month = self.semesters_voc[key][1]
                    return

        for key, value in self.months_voc.items():
            if key in self.quote_no_maj:
                count += 1
        if count == 1:
            for key, value in self.months_voc.items():
                if key in self.quote_no_maj:
                    self.start_month = self.months_voc[key]
                    self.end_month = self.months_voc[key]
                    break
        if count == 2:
            temp_count = 0
            for key, value in self.months_voc.items():
                if key in self.quote_no_maj:
                    temp_count += 1
                    if temp_count == 1:
                        self.start_month = self.months_voc[key]
                    elif temp_count == 2:
                        self.end_month = self.months_voc[key]

                    if temp_count == 2:
                        break

    def __get_tech(self):
        for key, value in self.tech_voc[self.broker_dict].items():
            if key in self.quote_no_maj:
                self.tech = self.tech_voc[self.broker_dict][key]
                break

    def __get_volume(self):
        no_volume = True
        if 'gw' in self.quote_no_maj:
            for num in map(int, [num.replace('-', '').replace(' ', '') for num in
                                 re.findall(r'\s\d+\s|\-\d+', self.quote_no_maj)]):
                if (num not in self.years_voc) & (isinstance(num, int)):
                    no_volume = False
                    self.volume = str(num * 1000)
                    break

        if 'mw' in self.quote_no_maj:
            numbers = map(int, re.findall(r'\d+', self.quote_no_maj))
            for num in numbers:
                if (num not in self.years_list) & (isinstance(num, int)):
                    no_volume = False
                    self.volume = str(num)
                    break
        if no_volume:
            self.volume = str(50000)

    def __get_price(self):
        self.price_request = not (any([isinstance(num, float) for num in map(float, [w.replace(',', '.') for w in
                                                                                     re.findall(r"\d+\,\d+",
                                                                                                self.quote_no_maj)])]) |
                                  any([isinstance(num, float) for num in map(float, [w.replace(',', '.') for w in
                                                                                     re.findall(r"\d+\.\d+",
                                                                                                self.quote_no_maj)])])) or 'request' in self.quote_no_maj

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\,\d+", self.quote_no_maj)]):
            if (num not in self.years_voc) & (isinstance(num, float)) & (num < 5):
                self.price = str(num)
                break

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\.\d+", self.quote_no_maj)]):
            if (num not in self.years_voc) & (isinstance(num, float)) & (num < 5):
                self.price = str(num)
                self.price = str(num)
                break

    def __get_label(self):
        for key, value in self.label[self.broker_dict].items():
            if key in self.quote_no_maj:
                self.label = self.label_voc[self.broker_dict][key]
                break
