from webdriver2 import Webdriver2
from textreader import Textreader
from quote_interpretor import Quote_Interpretor


def main():
    webdriver = Webdriver2()
    quote_interpretator = Quote_Interpretor()

    while True:
        print('   ')
        print('Enter your text')
        lines = []
        while True:
            line = input()
            if line:
                lines.append(line)
            else:
                break

        bad_quotes = []
        my_input = '\n'.join(lines)
        print (my_input)
        my_text = Textreader(mytext=my_input)
        counterC = 0
        for quote in my_text.list_of_quotes:
            try:

                print('   ')
                print('This is the current quote')
                print(quote)
                print('   ')
                print(my_text.list_of_quotes)

                # Uncomment if you want to use delNameDate... for now I have just changed the textinterpretor to do that for us automatically...
    #             print("Broker given?                ('y' for yes....any key for no)")
    #             x = input()
    #             if x=='y':
    #                 quote_interpretator.interprete(quote_interpretator.delNameDate(quote))
    #             else:
    #                 quote_interpretator.interprete(quote)

                quote_interpretator.interprete(quote)
                print ("passed quote_int")

                if len(my_text.list_of_quotes) > 1 and counterC == 0:
                    #no new window
                    webdriver.input_quote(quote_interpretator.new_quote)
                elif len(my_text.list_of_quotes) > 1:
                    #need new window
                    webdriver.switchWindow()
                    webdriver.input_quote(quote_interpretator.new_quote)
                else:
                    webdriver.input_quote(quote_interpretator.new_quote)


    # If just price is given without bidPrice or OfferPrice then input the regular way
                if (quote_interpretator.new_quote.bidPrice==0) and (quote_interpretator.new_quote.offerPrice==0):
                    webdriver.input_quote(quote_interpretator.new_quote)
                else:
                    # If both bid and offer are given then input the bid
                    webdriver.input_quote_bidOffer(quote_interpretator.new_quote, True)
                    # and then, in another form (in a new tab) input the offer
                    webdriver.switchWindow()
                    webdriver.input_quote_bidOffer(quote_interpretator.new_quote, False)
                print ("passed webdriver input")


            except Exception as e:
                print(e)
                bad_quotes.append(quote)

            counterC += 1

        print('Bad quotes:')
        print(bad_quotes)
            
            

    #test_method
    #while True:
    #   print('')
    #   print('Enter your text')
    #   lines = []

if __name__ == '__main__':
    main()
