import re
import collections

class Quote_Interpretor:
    def __init__(self):
        self.backHalfs = ['ERCOT GE BH22', 'ERCOT GE BH23', 'ERCOT GE BH24', 'ERCOT GE FH23', 'ERCOT GE FH24', 'ERCOT GE FH25']
    
        self.origin_voc = {
       ' ak ': 'Alaska',' as ': 'American Samoa', ' az ': 'Arizona', ' ca ': 'California', ' co ': 'Colorado', ' ct': 'Connecticut', ' dc': 'District of Columbia',
       ' de ': 'Delaware', ' fl ': 'Florida', ' ga ': 'Georgia', ' hi ': 'Hawaii', ' ia ': 'Iowa', ' id ': 'Idaho', ' il ': 'Illinois', ' in ': 'Indiana', ' ks ': 'Kansas', ' ky ': 'Kentucky',
       ' la ': 'Louisiana', ' ma ': 'Massachusetts', ' md ': 'Maryland', ' me ': 'Maine', ' mi ': 'Michigan', ' mn ': 'Minnesota', ' mo ': 'Missouri', ' mp ': 'Northern Mariana Islands', ' ms ': 'Mississippi',
       ' mt ': 'Montana', ' nc ': 'North Carolina', ' ne ': 'Nebraska', ' nh ': 'New Hampshire', ' nj ': 'New Jersey', ' nm ': 'New Mexico', ' nv ': 'Nevada', ' ny ': 'New York',
       ' oh ': 'Ohio', ' ok ': 'Oklahoma', ' or ': 'Oregon', ' pa ': 'Pennsylvania', ' pr ': 'Puerto Rico', ' sc ': 'South Carolina', ' sd ': 'South Dakota', ' tn ': 'Tennessee', ' tx ': 'Texas',
       ' ut ': 'Utah', ' va ': 'Virginia', ' vi ': 'Virgin Islands', ' vt ': 'Vermont', ' wa ': 'Washington', ' wi ': 'Wisconsin', ' wv ': 'West Virginia', ' wy ': 'Wyoming', ' ab ': 'Alberta', ' bc ': 'British Columbia',
       ' mb ': 'Manitoba', ' nb ': 'New Brunswick', ' nl ': 'Newfoundland and Labrador', ' nt ': 'Northwest Territories', ' ns ': 'Nova Scotia', ' nu ': 'Nunavut', ' on ': 'Ontario', ' pe ': 'Prince Edward Island',
       ' ma ': 'Massachusetts',' ny ': 'New York', ' sk ': 'Saskatchewan', ' yt ': 'Yukon', ' us ': 'United States', 'alaska': 'Alaska', 'alabama': 'Alabama', 'arkansas': 'Arkansas', 'american Samoa': 'American Samoa', 'arizona': 'Arizona', 'california': 'California',
       'colorado': 'Colorado', 'connecticut': 'Connecticut', 'district of columbia': 'District of Columbia','delaware': 'Delaware', 'florida': 'Florida', 'georgia': 'Georgia', 'guam': 'Guam', 'hawaii': 'Hawaii', 
       'iowa': 'Iowa','idaho': 'Idaho', 'illinois': 'Illinois', 'indiana': 'Indiana', 'kansas': 'Kansas', 'kentucky': 'Kentucky','louisiana': 'Louisiana', 'massachussets': 'Massachusetts', 'maryland': 'Maryland', 
       'maine': 'Maine', 'michigan': 'Michigan', 'minnesota': 'Minnesota', 'missouri': 'Missouri', 'mariana': 'Northern Mariana Islands', 'mississippi': 'Mississippi', 
       'montana': 'Montana', 'north carolina': 'North Carolina', 'north dakota': 'North Dakota', 'nebraska': 'Nebraska', 'new hamsphire': 'New Hampshire', 'new jersey': 'New Jersey', 'new mexico': 'New Mexico', 
       'nevada': 'Nevada', 'new york': 'New York', 'ohio': 'Ohio', 'oklahoma': 'Oklahoma', 'oregon': 'Oregon', 'pennsylvania': 'Pennsylvania', 'puerto rico': 'Puerto Rico', 'rhode island': 'Rhode Island', 
       'south carolina': 'South Carolina', 'south dakota': 'South Dakota', 'tennessee': 'Tennessee', 'texas': 'Texas', 'utah': 'Utah', 'virginia': 'Virginia', 'virgin islands': 'Virgin Islands', 'vermont': 'Vermont', 
       'washington': 'Washington', 'wisconsin': 'Wisconsin', 'west virginia': 'West Virginia', 'wyoming': 'Wyoming', 'alberta': 'Alberta', 'british columbia': 'British Columbia', 
       'manitoba': 'Manitoba', 'brunswick': 'New Brunswick', 'newfoundland': 'Newfoundland and Labrador', 'northwest': 'Northwest Territories', 'nova': 'Nova Scotia', 'nunavut': 'Nunavut', 'ontario': 'Ontario',
       'prince edward': 'Prince Edward Island', 'quebec': 'Quebec', 'saskatchewan': 'Saskatchewan', 'yukon': 'Yukon', 'pjm tri': ['Maryland', 'New Jersey', 'Pennsylvania'], 'pjm 1': ['Maryland', 'New Jersey', 'Pennsylvania'], 'pjm1': ['Maryland', 'New Jersey', 'Pennsylvania'],
        'nepool 1': ['Connecticut','Massachusetts','New Hampshire','Rhode Island']}

        self.compliance_voc = {'pjm' : ['Jun', 'May'], 'pa ' : ['Jun', 'May'], 'pa1' : ['Jun', 'May'], 'pa2' : ['Jun', 'May'],
                               'nj' : ['Jun', 'May'], 'dc' : ['Jun', 'May']}

        self.years_voc = {'2021': '2021', '2022': '2022', '2023': '2023', '2024': '2024','2025': '2025',
                          '2026': '2026', '2027': '2027', '2028': '2028', '2029': '2029', '2030': '2030',
                          '-21': '2021', '-22': '2022', '-23': '2023', '-24': '2024', '-25': '2025', '-26': '2026', '-27': '2027',
                          '-28': '2028', '-29': '2029', '-30': '2030',
                          '21': '2021', '22': '2022', '23': '2023', '24': '2024', '25': '2025', '26': '2026', '27': '2027',
                          '28': '2028', '29': '2029', '30': '2030'}
    
        self.months_voc = {'jan': 'Jan', 'feb': 'Feb', 'mar': 'Mar', 'apr': 'Apr', 'may': 'May', 'jun': 'Jun',
                           'jul': 'Jul', ' aug': 'Aug', 'sep': 'Sep', 'oct': 'Oct', 'nov': 'Nov', 'dec': 'Dec'}

        self.quarters_voc = {'q1': ['Jan', 'Mar'], 'q2': ['Apr', 'Jun'], 'q3': ['Jul', 'Sep'], 'q4': ['Oct', 'Dec']}

        self.semesters_voc = {'bh': ['July', 'December'], 'fh': ['January', 'June'],'cal': ['January', 'December'],'ry': ['July', 'December'], '2h': ['July', 'December'], '1h': ['January', 'June']}

        self.years_list = [2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2027, 2028, 2029, 2030]
        self.tech_voc = {'any': 'Any','Any': 'Any','Hydro': 'Hydro','hydro': 'Hydro','Wind': 'Wind', 'wind': 'Wind', 'wnd': 'Wind', 'SREC': 'Solar', 'solar': 'Solar','Solar': 'Solar','biogas': 'Biogas','Biogas': 'Biogas', 'biomass': 'Biomass',
                         'bio': 'Biomass','Biomass': 'Biomass','Non-Waste': 'Non-Waste','non-waste': 'Non-Waste','Waste Coal': 'Waste Coal','waste coal': 'Waste Coal', 'sol':'Solar'}

        self.label_voc = {'lcfs': 'CA LCFS Eligible','cco': ' CCO 0', 'cec': 'CEC PCC3','pcc3': 'CEC PCC3','pcc1': 'CA RPS PCC1',
                          '< 5 yrs': 'COD <5 yrs','<5 yrs': 'COD <5 yrs','<5yrs': 'COD <5 yrs','crs': 'CRS Listed',
                          'ge': 'CRS Listed','g-e': 'CRS Listed','green': 'CRS Listed', 'eco': 'Ecologo', 'aps':'MA APS', 
                          'epa': 'EPA Green Power Partnership Eligible', 'green power':'EPA Green Power Partnership Eligible',
                          'ct1': 'CT Class 1','ct 1': 'CT Class 1','ctc1': 'CT Class 1','ct c1': 'CT Class 1','ct class 1': 'CT Class 1',
                          'ct2': 'CT Class 2','ct 2': 'CT Class 2','ctc2': 'CT Class 2','ct c2': 'CT Class 2','ct class 2': 'CT Class 2',
                          'dc1': 'DC Class 1 RPS','dc 1': 'DC Class 1 RPS','dcc1': 'DC Class 1 RPS','dc c1': 'DC Class 1 RPS','dc class 1': 'DC Class 1 RPS',
                          'ma1': 'MA Class 1','ma 1': 'MA Class 1','mac1': 'MA Class 1','ma c1': 'MA Class 1','ma class 1': 'MA Class 1',
                          'ma2': 'MA Class 2 Non Waste','ma 2': 'MA Class 2 Non Waste','mac2': 'MA Class 2 Non Waste','ma c2': 'MA Class 2 Non Waste','ma class 2': 'MA Class 2 Non Waste',
                          'me1': 'ME Class 1A','me 1': 'ME Class 1A','mec1': 'ME Class 1A','me c1': 'ME Class 1A','me class 1': 'ME Class 1A',
                          'nj c2':'NJ C2','nj2':'NJ C2','nj 2':'NJ C2',
                          'pa c1': 'PA T1', 'pa1': 'PA T1', 'pa 1': 'PA T1',
                          'pa c2':'PA T2','pa2':'PA T2','pa 2':'PA T2',
                          'pa solar': 'PA SREC', 'pa sol': 'PA SREC', 'pa srec': 'PA SREC',
                          'nj srec': 'NJ SREC', 'nj solar': 'NJ SREC', 'ma srec 1': 'MA SREC Class 1', 'ma srec c1': 'MA SREC Class 1',
                          'nepool 1': ['CT Class 1','MA Class 1','NH Class 1','RI New'],
                          'tri ': ['PA T1', 'NJ Class I', 'MD Tier I'], 'pjm 1': ['PA T1', 'NJ Class I', 'MD Tier I'], 'pjm1': ['PA T1', 'NJ Class I', 'MD Tier I'],
                          'md tier ii':'MD Tier 2', 'md2':'MD Tier 2','md tier 2':'MD Tier 2',
                          'or lcfs': 'Oregon Clean Fuel Eligible', 'oregon lcfs': 'Oregon Clean Fuel Eligible',
                          'md1': 'MD Tier I', 'md 1': 'MD Tier I',
                          'ny edp': 'NY EDP Eligible', 'va': 'Virginia RPS', 'va dg': 'Virginia RPS DG', 'virginia dg': 'Virginia RPS DG',
                          'oh': 'Ohio Non-Solar RPS',
                          'me1': 'Maine Class 1', 'me 1': 'Maine Class 1', '1a': 'Maine Class 1A', 'me2': 'Maine Class 2', 'me 2': 'Maine Class 2',
                           }
        
        # I added ERCOT GE BH23 as a proxy if we are given bh23 or 2h23... but that proxy is not showing up in datahive yet... must be 
        # relatively new... It prevents Ercot bh23 from being selected... should I remove these for that reason... 
        self.proxy_voc_old = {'2H23':'ERCOT GE BH23','2h23':'ERCOT GE BH23','bh23':'ERCOT GE BH23','BH23':'ERCOT GE BH23','hydro':'US Hydro','MA I':'NEPOOL MA C1','cel':'CEL Spot','vcs':'US VCS > 3 years any tech','cca':'CA Carbon Offset','california carbon':'CA Carbon Offset','ca lcfs':'CA LCFS Credit','corsia':'Corsia USD', 'nar ge wind': 'National GE Wind', 'nar g-e wind': 'National GE Wind','tx':'ERCOT GE','ercot':'ERCOT GE','tx solar':'ERCOT GE Solar','tx - ge':'ERCOT GE','tx crs':'ERCOT GE', 'ercot - ge':'ERCOT GE','il wind':'IL WIND','national any':'National GE ANY','ge wind':'National GE WIND','lcfs':'LCFS REC', 'texas':'ERCOT GE'
        ,'irec':'I-REC','i-rec':'I-REC','c3':'NEPOOL NH C3','rps':'NEPOOL RPS C1','ct c1':'CT C1','ct c2':'CT C2','nh c1':'NH C1 Thermal','ma aps':'NEPOOL MA APS','ma srec 1':'NEPOOL MA SREC 1','ma srec 2':'NEPOOL MA SREC 2'
        ,'me c1':'NEPOOL ME C1','me c2':'NEPOOL ME C2','nh c2':'NEPOOL NH C2','nh c4':'NEPOOL NH C4','ny any':'NY ANY','ny wind':'NY WIND','pjm tri':'PJM C1','tri':'PJM C1','pjm oh':'PJM OH NONSOLAR','pjm c2':'PJM C2','dc c1':'PJM DC C1'
        ,'md c2':'PJM MD C2','nj c2':'PJM NJ C2','nj2':'PJM NJ C2','nj 2':'PJM NJ C2','pa c2':'PJM PA C2','pa2':'PJM PA C2','pa c1':'PJM PA C1','pa 1':'PJM PA C1','pa1':'PJM PA C1','mn ss':'MRETS MN SS','md srec':'PJM MD SREC','nj solar':'PJM NJ SREC','nj srec':'PJM NJ SREC','pa srec':'PJM PA SREC','national any':'National ANY','national wind':'National WIND'
        ,'rec':'National ANY','cec':'CEC','pnw':'PNW','wa dg':'PNW WA DG','wecc':'WECC','wecc ge':'WECC GE', "ge wind": "National GE Wind", "green e wind": "National GE Wind", "greene wind": "National GE Wind", 'ge nar wind': 'National GE Wind'
        ,'pa ii': 'PJM PA C2','dc1': 'PJM DC C1','dc 1': 'PJM DC C1','dcc1': 'PJM DC C1','dc c1': 'PJM DC C1','dc class 1': 'PJM DC C1', 'md tier ii': 'PJM MD C2','md1': 'PJM MD C1','md tier i': 'PJM MD C1','md c1': 'PJM MD C2','tx ge sol': 'ERCOT GE Solar'}   
        
        self.proxy_voc = {
        'nepool 1': 'NEPOOL C1 Quad',
            
        '2h22': 'ERCOT GE BH22',
        'bh22': 'ERCOT GE BH22',
        'bh22 tx': 'ERCOT GE BH22',

        '1h23': 'ERCOT GE FH23',
        'fh23': 'ERCOT GE FH23',
        'fh23 tx': 'ERCOT GE FH23',

        '2h23': 'ERCOT GE BH23',
        'bh23': 'ERCOT GE BH23',
        'bh23 tx': 'ERCOT GE BH23',

        '1h24': 'ERCOT GE FH24',
        'fh24': 'ERCOT GE FH24',
        'fh24 tx': 'ERCOT GE FH24',

        '2h24': 'ERCOT GE BH24',
        'bh24': 'ERCOT GE BH24',
        'bh24 tx': 'ERCOT GE BH24',

        '1h25': 'ERCOT GE FH25',
        'fh25': 'ERCOT GE FH25',
        'fh25 tx': 'ERCOT GE FH25',

        '2h25': 'ERCOT GE BH25',
        'bh25': 'ERCOT GE BH25',
        'bh25 tx': 'ERCOT GE BH25',

        'hydro': 'US Hydro',

        'MA I': 'NEPOOL MA C1',
        'MA 1': 'NEPOOL MA C1',
        'ma i': 'NEPOOL MA C1',
        'ma 1': 'NEPOOL MA C1',
        'ma c1': 'NEPOOL MA C1',
        'ma 1/ct 1': 'NEPOOL C1 Dual',
        'ma 1 ct 1': 'NEPOOL C1 Dual',
        'ma1/ct1': 'NEPOOL C1 Dual',
        'ma1 ct1': 'NEPOOL C1 Dual',

        'cel': 'CEL Spot',
        'vcs': 'US VCS > 3 years any tech',
        'cca': 'CA Carbon Offset',
        'california carbon': 'CA Carbon Offset',
        'ca carbon': 'CA Carbon Offset',

        'corsia': 'Corsia USD',
        'national any': 'National ANY',

        'us ge wnd': 'National GE Wind',

        'us ge wind': 'National GE Wind',
        'g-e wind': 'National GE Wind',
        'green e wind': 'National GE Wind',
        'greene wind': 'National GE Wind',
        'green-e wind': 'National GE Wind',

        'ge nar wind': 'National GE Wind',
        'g-e nar wind': 'National GE Wind',
        'greene nar wind': 'National GE Wind',
        'green-e nar wind': 'National GE Wind',
        'green e nar wind': 'National GE Wind',

        'nar ge wind': 'National GE Wind',
        'nar g-e wind': 'National GE Wind',
        'nar green-e wind': 'National GE Wind',
        'nar green e wind': 'National GE Wind',
        'nar greene wind': 'National GE Wind',

        'us ge solar': 'National GE ANY',

        'tx': 'ERCOT GE Ratable',
        'tx - ge': 'ERCOT GE Ratable',
        'tx ge': 'ERCOT GE Ratable',
        'tx - crs': 'ERCOT GE Ratable',
        'tx crs': 'ERCOT GE Ratable',

        'ercot': 'ERCOT GE Ratable',
        'ercot - ge': 'ERCOT GE Ratable',
        'ercot ge': 'ERCOT GE Ratable',
        'ercot - crs': 'ERCOT GE Ratable',
        'ercot crs': 'ERCOT GE Ratable',

        'texas': 'ERCOT GE Ratable',
        'texas - ge': 'ERCOT GE Ratable',
        'texas ge': 'ERCOT GE Ratable',
        'texas - crs': 'ERCOT GE Ratable',
        'texas crs': 'ERCOT GE Ratable',

        'tx solar': 'ERCOT GE Solar',
        'texas solar': 'ERCOT GE Solar',

        'tx ge sol': 'ERCOT GE Solar',
        'texas ge sol': 'ERCOT GE Solar', 

        'tx sol': 'ERCOT GE Solar',
        'texas sol': 'ERCOT GE Solar',

        'tx greene sol': 'ERCOT GE Solar',
        'tx green-e sol': 'ERCOT GE Solar',
        'tx green e sol':'ERCOT GE Solar',
        'texas greene sol': 'ERCOT GE Solar',
        'texas green-e sol': 'ERCOT GE Solar',
        'texas green e sol':'ERCOT GE Solar',

        'il wind': 'IL WIND',
        'illinois wind': 'IL WIND',    
        'il wnd': 'IL WIND',

        # The current version of the tool does not have CA LCFS Credits... so we won't develop this part too much... 
        # Plus I'm gonna make this default to lcfs recs instead of lcfs credits... 
        'ca lcfs credit': 'CA LCFS Credit',
        'california lcfs credit': 'CA LCFS Credit',
        'cali lcfs credit': 'CA LCFS Credit',
        'lcfs credit': 'CA LCFS Credit',

        'lcfs': 'LCFS REC',
        'lcfs rec': 'LCFS REC',
        'ca': 'LCFS REC',
        'ca rec': 'LCFS REC',
        'california rec': 'LCFS REC',
        'cali rec':'LCFS REC',
        'ca lcfs': 'CA LCFS Credit',
        'california lcfs': 'CA LCFS Credit',
        'cali lcfs': 'CA LCFS Credit',

        'irec': 'I-REC',
        'i-rec': 'I-REC',

        # New Hampshire RECs... I bet we do not deal with these a lot so the reasoning here can be a little 
        # more simplistic... 
        # NH C1 Thermal is a label, I think... not a proxy.. I've changed it to NEPOOL NH C1... although there is no option for this in the tool
        'nh c3': 'NEPOOL NH C3',
        'nh c2': 'NEPOOL NH C2',
        'nh c4': 'NEPOOL NH C4',
        'nh c1': 'NEPOOL NH C1',

        # NEPOOL is not the only rps product anymore... But, I think it is traded more... so I'll still default to it unless ny rps is specified.... 
        'rps': 'NEPOOL RPS C1',
        'ny rps': 'NYC RPS C1',
        'nepool rps': 'NEPOOL RPS C1',


        'ct c1': 'CT C1',
        'ct c2': 'CT C2',
        'connecticut c1': 'CT C1',
        'connecticut c2': 'CT C2',
        'connecticut class 1': 'CT C1',
        'connecticut class 2': 'CT C2',


        # ^These are fine...

        'ma aps': 'NEPOOL MA APS',
        'ma ces-e': 'NEPOOL MA CES-E',
        'ma ces e': 'NEPOOL MA CES-E',
        'ma ces': 'NEPOOL MA CES',
        'ma srec 1': 'NEPOOL MA SREC 1',
        'ma srec 2': 'NEPOOL MA SREC 2',
        'me c1': 'NEPOOL ME C1',
        'me 1': 'NEPOOL ME C1',
        'me c2': 'NEPOOL ME C2',
        'me 2': 'NEPOOL ME C2',
        'maine 2': 'NEPOOL ME C2',

        'ny any': 'NY ANY',
        'ny edp': 'NEPOOL NY EDP',
        'ny wind': 'NY WIND',
        'pjm tri': 'PJM C1',
        'pjm 1': 'PJM C1',
        'tri': 'PJM C1',
        'pjm oh': 'PJM OH NONSOLAR',
        'pjm c2': 'PJM C2',
        'dc c1': 'PJM DC C1',
        'md c2': 'PJM MD C2',
        'nj c2': 'PJM NJ C2',
        'nj2': 'PJM NJ C2',
        'nj 2': 'PJM NJ C2',
        'pa c2': 'PJM PA C2',
        'pa2': 'PJM PA C2',
        'pa c1': 'PJM PA C1',
        'pa 1': 'PJM PA C1',
        'pa1': 'PJM PA C1',
        'mn ss': 'MRETS MN SS',
        'md srec': 'PJM MD SREC',
        'nj solar': 'PJM NJ SREC',
        'nj srec': 'PJM NJ SREC',
        'pa srec': 'PJM PA SREC',
        'pa solar': 'PJM PA SREC',
        'pa sol': 'PJM PA SREC',
        'md 2': 'PJM MD C2',
        'md2': 'PJM MD C2',

        'national wind': 'National WIND',
        'rec': 'National ANY',
        'cec': 'CEC',
        'pnw': 'PNW',
        'wa dg': 'PNW WA DG',
        'wecc': 'WECC',
        'wecc ge': 'WECC GE',

        'pa ii': 'PJM PA C2',
        'pa 2': 'PJM PA C2',
        'dc1': 'PJM DC C1',
        'dc 1': 'PJM DC C1',
        'dcc1': 'PJM DC C1',
        'dc class 1': 'PJM DC C1',
        'md tier ii': 'PJM MD C2',
        'md 1': 'PJM MD C1',
        'md tier i': 'PJM MD C1',
        'md c1': 'PJM MD C2',

        'pcc3': 'CEC',

        'va': 'PJM VA Renewable',
        'va dg': 'PJM VA Distributed',

        'oh rec': 'PJM OH NONSOLAR',
        'oh': 'PJM OH NONSOLAR'
        }


# Could we have a 

        self.registry_voc = {'ercot': 'ERCOT',
                             'nar':'NARR',
                             'narr':'NARR',
                             'mrets':'M-RETS',
                             'm-rets':'M-RETS',
                             'm rets':'M-RETS',
                             'wregis':'WREGIS',
                             'pjm-gats':'PJM-GATS',
                             'pjm gats':'PJM-GATS',
                             'pjm':'PJM-GATS',
                             'nepool':'NEPOOL',
                             'nodal': 'Nodal',
                             'ndl': 'Nodal',
                             'ice': 'ICE'}
        
        
    # Add a dictionary of counterparties here...
        self.broker_voc = {'seth twisselmann': 'IVG Energy LTD',
                           'john schiling': 'IVG Energy LTD',
                           'patrick mcfadden': 'Spectron Energy',
                           'nicholas wells': 'GFINET',
                           'zeyd tabbara': 'BGC Environmental Brokerage Services LP',
                           'nicole shaughnessy': 'BGC Environmental Brokerage Services LP',
                           'jake lewis': 'IVG Energy LTD',
                           'doug edwards': 'Tullett Prebon Americas Corp',
                           'eric scala': 'Tullett Prebon Americas Corp',
                           'brian bogusevic': 'Tullett Prebon Americas Corp',
                           'evan redding': 'BGC Environmental Brokerage Services LP',
                           'eric klein': 'TFS Energy Futures',
                           'peter costantinou': 'Tullett Prebon Americas Corp',
                           'dave mann': 'TFS Energy Futures',
                           'rizzo': 'Clear Energy Brokerage & Consulting LLC',
                           'lauren kisling': 'BGC Environmental Brokerage Services LP',
                           'jack velasquez': 'Spectron Energy',
                           'thomas gibson': 'GFINET',
                           'richard heffernan': 'Trident Brokerage Services',
                           'tyler saito': 'Tullett Prebon Americas Corp'
                            }

# Registry path does not work consistently 

                       
                       
          

    def interprete(self, quote):
    
        self.quote = quote
        self.quote_no_maj = self.quote.lower()
        self.new_quote = Quote()
        self.new_quote.raw = quote
        self.__get_price()
        self.__is_price_request()
        self.__get_year()
        self.__get_months()
        self.__get_broker()
        self.__get_bidoffer()
        self.__get_tech()
        self.__get_label()
        self.__get_proxy()
        self.__get_origin()
        self.__get_volume()
        self.__get_registry()
        self.__get_counterparty()
        

    def delNameDate(self,quote): 
        try: #Remove name and date if it is there
            print("The quote was inputted as: ")
            print((quote.replace(re.findall("\s[A-Z][A-Z]\s[A-Z]+[a-z]*\s[A-Z]+[a-z]*\s", str(quote))[0], '')))
            return (quote.replace(re.findall("\s[A-Z][A-Z]\s[A-Z]+[a-z]*\s[A-Z]+[a-z]*\s", str(quote))[0], ''))
        except: #Else just return the quote as is
            return quote
        

    def __get_bidoffer(self):
        if 'offer' in self.quote_no_maj:
            self.new_quote.quote_type = 'offer'
        if '@' in self.quote_no_maj:
            self.new_quote.quote_type = 'offer'
        if 'bid' in self.quote_no_maj:
            self.new_quote.quote_type = 'bid'
        if 'looking for' in self.quote_no_maj and 'offer' in self.quote_no_maj:
            self.new_quote.quote_type = 'bid'
        if 'looking for' in self.quote_no_maj and 'bid' in self.quote_no_maj:
            self.new_quote.quote_type = 'offer'

    def __get_broker(self):
        #fill in with US Brokers
#         self.new_quote.broker = 'New Counterparty'*****************************
        self.new_quote.broker = ''
        if 'doug edwards' in self.quote:
            self.new_quote.broker = 'Tullet Prebon Americas Corp'
        if 'icap' in self.quote:
            self.new_quote.broker = 'Tullet Prebon Americas Corp'
        if 'TP' in self.quote:
            self.new_quote.broker = 'Tullet Prebon Americas Corp'
        if 'eric scala' in self.quote:
            self.new_quote.broker = 'Tullet Prebon Americas Corp'
        if 'bgc' in self.quote:
            self.new_quote.broker = 'BGC Environmental Brokerage Services LP'
        if 'evan redding' in self.quote:
            self.new_quote.broker = 'BGC Environmental Brokerage Services LP'
        if 'brian bogusevic' in self.quote:
            self.new_quote.broker = 'Tullet Prebon Americas Corp'
        if 'clear' in self.quote:
            self.new_quote.broker = 'Clear Energy'
        if 'nicole shaughnessy' in self.quote:
            self.new_quote.broker = 'BGC Environmental Brokerage Services LP'
        if 'jake lewis' in self.quote:
            self.new_quote.broker = 'IVG Energy LTD'
        if 'nicholas wells' in self.quote:
            self.new_quote.broker = 'GFINET'
        elif 'michael rizzo' in self.quote:
            self.new_quote.broker = 'Clear Energy'
            
            
    def countFreq(self, pat, txt):
        M = len(pat)
        N = len(txt)
        res = 0
 
    # A loop to slide pat[] one by one
        for i in range(N - M + 1):
 
        # For current index i, check
        # for pattern match
            j = 0
            while j < M:
                if (txt[i + j] != pat[j]):
                    break
                j += 1
 
            if (j == M):
                res += 1
                j = 0
        return res

    def __get_origin(self):
        self.new_quote.origin = []
        tempList = {}
        for key, value in self.origin_voc.items():
            if (key!='id ') and (key in self.quote_no_maj):
                tempList[key] = self.origin_voc[key]
                if type(self.origin_voc[key]) is list:
                    self.new_quote.origin.extend(self.origin_voc[key])
                else:
                    self.new_quote.origin.append(self.origin_voc[key])
            # Earlier, the 'id,' in 'bid,' was being recognized as Idaho... this code block fixes that...
            elif (key=='id ') and (key in self.quote_no_maj) and (self.countFreq('id ', self.quote_no_maj)>self.countFreq('bid',self.quote_no_maj)):
                tempList[key] = self.origin_voc[key]
                self.new_quote.origin.append(self.origin_voc[key])
        print(tempList)

        # Make origins list unique
        self.new_quote.origin = list(set(self.new_quote.origin))
        print(self.new_quote.origin)

                         
        # Is this still necessary
        # Earlier, when you input Yukon... it recognized the on as ontario... this code block fixes this by considering only the region
        # which has a larger key length (i.e only 'Yukon' not 'on')
        #if len(tempList)>1:
        #    tempMax = ""
        #    for origin in tempList.keys():
        #        if len(origin)>len(tempMax):
        #            tempMax = origin
        #    self.new_quote.origin = self.origin_voc[tempMax]
         
    def __get_origin_old(self):
        for key, value in self.origin_voc.items():
            if key in self.quote_no_maj:            
                self.new_quote.origin = self.origin_voc[key]
                break
    # The proxy started breaking when I added this?
    def __get_year(self):
        prices=[]
        volumes = []
        potentialYears = []
        years = []
        
        for num in map(float, [num.replace('-', '').replace(' ', '') for num in re.findall(r"[-+]?(?:\d*\.\d+|\d+)", self.quote)]):
            if num.is_integer(): 
                potentialYears.append(str(int(num)))
                if len(re.findall(r"[-+]?(?:\d*\.\d+|\d+)\s?.mwh?" ,  self.quote)) != 0:
                    if (re.findall(r"[-+]?(?:\d*\.\d+|\d+)\s?.mwh?" , self.quote)[0][:2] == str(int(num))):
                        volumes.append(str(int(num)))

                if (len((re.findall(r"[-+]?(?:\d*\.\d+|\d+)\s?k" , self.quote))) != 0):
                    if (re.findall(r"[-+]?(?:\d*\.\d+|\d+)\s?k" , self.quote)[0][:2] == str(int(num))):
                        volumes.append(str(int(num)))

        if (len(re.findall(r"\$\s?[-+]?(?:\d*\.\d+|\d+)" , self.quote)) != 0):
            for price in re.findall(r"\$\s?[-+]?(?:\d*\.\d+|\d+)" , self.quote):
                prices+= re.findall(r"[-+]?(?:\d*\.\d+|\d+)", price)

        if (len(re.findall(r"\@\s?[-+]?(?:\d*\.\d+|\d+)" , self.quote)) != 0):
            for price in re.findall(r"\@\s?[-+]?(?:\d*\.\d+|\d+)" , self.quote):
                prices+= re.findall(r"[-+]?(?:\d*\.\d+|\d+)", price)
                
        testing = []
        # This line is not working... 
        for key, value in self.years_voc.items():
            testing.append(key)
            if (key in potentialYears) and (key not in volumes) and (key not in prices):
                years.append(value)
                
        # This is wrong... we dont need to go into years voc to set the years... we could do it just like that... 
        # Oh well... this should give the right answer as well... 

        if len(years) == 2:
            years.sort()
            print("YEARS")
            print(years)
            self.new_quote.start_year = years[0]
            # For some reason, this print statement prevents the start year from getting lost somehow? 
            print(self.new_quote.start_year)
            self.new_quote.end_year =years[1]
        else:
            # It is fucking up over here... nothing is being appended to years... lets test this quote in our debugging
            # It looks fine in debugging... why is it messing up here then... 
            self.new_quote.start_year = years[0]
            self.new_quote.end_year = years[0]
       
    
    
    
    
    
    
    
        # UNDER CONSTRUCTION HERE... 
        # FIX THIS: this will fuck up if there is a name like toRY in the quote... 
        
        # Thi is not working correctly... we need to be able to account for 'RY' as well... 
        
        if 'ry' in self.quote:
            self.new_quote.start_year = str(int(self.new_quote.start_year) - 1) 
            self.new_quote.end_year = str(int(self.new_quote.end_year))
    
    
    def __get_year_old(self):
        for key, value in self.years_voc.items():
            if key in self.quote:
                self.new_quote.start_year = self.years_voc[key]
                self.new_quote.end_year = self.years_voc[key]
                if self.new_quote.end_year == str(2031):
                    print('aa')

        if 'ry' in self.quote_no_maj:
            self.new_quote.start_year = str(int(self.new_quote.start_year) - 1) 
            self.new_quote.end_year = str(int(self.new_quote.end_year))
       

    def __get_months(self):
        count = 0

        is_quarter = any([key in self.quote_no_maj for key, value in self.quarters_voc.items()])
        is_semester = any([key in self.quote_no_maj for key, value in self.semesters_voc.items()])
        is_compliance = any([key in self.quote_no_maj for key, value in self.compliance_voc.items()])

        if is_compliance:
            for key, value in self.compliance_voc.items():
                if key in self.quote_no_maj:
                    self.new_quote.start_year = str(int(self.new_quote.start_year) - 1)
                    self.new_quote.start_month = self.compliance_voc[key][0]
                    self.new_quote.end_month = self.compliance_voc[key][1]
                    return

        if is_quarter:
            for key, value in self.quarters_voc.items():
                if key in self.quote_no_maj:
                    self.new_quote.start_month = self.quarters_voc[key][0]
                    self.new_quote.end_month = self.quarters_voc[key][1]
                    return

        if is_semester:
            for key, value in self.semesters_voc.items():
                if key in self.quote_no_maj:
                    self.new_quote.start_month = self.semesters_voc[key][0]
                    self.new_quote.end_month = self.semesters_voc[key][1]
                    return

        for key, value in self.months_voc.items():
            if key in self.quote_no_maj:
                count += 1
        if count == 1:
            for key, value in self.months_voc.items():
                if key in self.quote_no_maj:
                    self.new_quote.start_month = self.months_voc[key]
                    self.new_quote.end_month = self.months_voc[key]
                    break
        if count == 2:
            temp_count = 0
            for key, value in self.months_voc.items():
                if key in self.quote_no_maj:
                    temp_count += 1
                    if temp_count == 1:
                        self.new_quote.start_month = self.months_voc[key]
                    elif temp_count == 2:
                        self.new_quote.end_month = self.months_voc[key]

                    if temp_count == 2:
                        break

    def __get_tech_old(self):
        counter = 0 
        self.new_quote.tech = []
        for key, value in self.tech_voc.items():
            if key in self.quote_no_maj:
                self.new_quote.tech.append(self.tech_voc[key])
                counter += 1 
        if counter == 0:
            self.new_quote.tech.append("Any")

        if 'non-bio' in self.quote_no_maj:
            self.new_quote.tech = ['Hydro', 'Wind', 'Solar', 'Geothermal']
            



# Perhaps I should treat cases like bio or biomass and sol or solar separately... I can first identify them... and then... I can 
# just write that stuff into my code... 
    def __get_tech(self):
        counter = 0 
        smallOnes = {'bio': 'Biomass', 'sol':'Solar'}
        self.new_quote.tech = []
        copyDict = {}
        for key, value in self.tech_voc.items():
            if key in self.quote_no_maj:

                # if (('Solar' not in newTech) and (key=='sol')) or (('Solar' not in newTech) and (key=='sol'))
                self.new_quote.tech.append(self.tech_voc[key])
                copyDict[key] = value
                counter += 1 
        if counter == 0:
            self.new_quote.tech.append("Any")
            copyDict["any"] = "Any"
           
        if len(self.new_quote.tech)>1:
            # deal with duplicates... this will allow us to have only one tech registered for quotes like "solar sol sol SOLAR" etc. 
            flipped={}
            for k, v in copyDict.items():
                if v not in flipped:
                    flipped[v]=[k]
                else:
                    flipped[v].append(k)
            
            if 'Biogas' in flipped.keys():
                # Check how many instances of biomass there are... if there are two then let it remain... if there is only one then 
                # remove biomass from the dictionary... 
                for k,v in flipped.copy().items():
                    if (k=='Biomass') and (len(v)>1):
                        continue
                    elif (k=="Biomass") and (len(v)==1):
                        del flipped[k]
                    
            for k, v in flipped.items():
                flipped[k] = max(v, key=len)
            self.new_quote.tech=list(flipped.keys())
                    
        if 'non-bio' in self.quote_no_maj:
            self.new_quote.tech = ['Hydro', 'Wind', 'Solar', 'Geothermal']

    def __get_volume_old(self):
        no_volume = True
        if ('gw' in self.quote_no_maj):
            try:
                year_to_delete = re.findall('(\d*\.?\d+) *(year)', self.quote_no_maj)[0][0]
                temp_quote = self.quote_no_maj.replace(' ' + str(year_to_delete) + ' ',
                                                       '')  # dirty trick to delete the year number in a quote like Interest to buy Q4-20 EU/NO Hydro < 6 years, 13 GWh.
            except:
                temp_quote = self.quote_no_maj
            for num in map(int, [num.replace('-', '').replace(' ', '') for num in
                                 re.findall(r'\s\d+\s', temp_quote)]):
                if (num not in self.years_list) & (isinstance(num, int)):
                    no_volume = False
                    self.new_quote.volume = str(num * 1000)
                    return
#******************************************************************************************************************
        if ('k' in self.quote_no_maj):
            position = self.quote_no_maj.find('k') - 1 
            check = self.quote_no_maj[position].isdigit()
            if check == True:              
                try:
                    year_to_delete = re.findall('(\d*\.?\d+) *(year)', self.quote_no_maj)[0][0]
                    temp_quote = self.quote_no_maj.replace(' ' + str(year_to_delete) + ' ','')  
                except:
                    temp_quote = self.quote_no_maj
                for num in map(int, [num.replace('-', '').replace(' ', '') for num in re.findall(r'\s\d+', temp_quote)]):
                    if (num not in self.years_list) & (isinstance(num, int)):
                        no_volume = False
                        self.new_quote.volume = str(num * 1000)
                        return
#*******************************************************************************************************************
        if 'mw' in self.quote_no_maj:
            numbers = map(int, re.findall(r'\d+', self.quote_no_maj))
            for num in numbers:
                if (num not in self.years_list) & (isinstance(num, int)):
                    no_volume = False
                    self.new_quote.volume = str(num)
                    return
        if no_volume:
            self.new_quote.volume = str(50000)
            
    def __get_volume_old2(self):
        yearsList = [2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2027, 2028, 2029, 2030]
        sqn = self.quote_no_maj
        sqnVolume = 0
        temp_quote = ""
        no_volume = True
        hasK = False

        if ('k' in sqn):
            position = sqn.find('k') - 1 
            check = sqn[position].isdigit()
            if check == True:
                hasK = True
        try:
            year_to_delete = re.findall('(\d*\.?\d+) *(year)', sqn)[0][0]
            print(year_to_delete)
            temp_quote = sqn.replace(' ' + str(year_to_delete) + ' ','')  
        except:
            temp_quote = sqn

        for num in map(float, [num.replace('-', '').replace(' ', '') for num in re.findall(r"[-+]?(?:\d*\.\d+|\d+)", temp_quote)]):
            aYear1 = ((str(int(num)) in list(self.years_voc.keys())))
            aYear2 = ((str(int(num))+" " in list(self.years_voc.keys())))
            aYear=False
            if True in [aYear1, aYear2]:
                aYear = True

            if (num.is_integer()) and not aYear:

                no_volume = False
                if ('gw' in sqn):
                    if hasK:
                        sqnVolume = str(int(num) * 1000*1000)
                    else:
                        sqnVolume = str(int(num) *1000)
                elif ('mw' in sqn):
                    if hasK:
                        sqnVolume = str(int(num) * 1000)  
                    else:
                        sqnVolume = str(int(num))
                else:
                    if hasK:
                        sqnVolume = str(int(num) * 1000)
                    else: 
                        sqnVolume = str(int(num))
        if no_volume:
            sqnVolume = str(50000)
        self.new_quote.volume =  sqnVolume
        
    def findOccurrences(self, s, ch):
        return [i for i, letter in enumerate(s) if letter == ch]
    
    
    # This volume function is too roundabout... we need to make it simpler... lets debug it using the ideas we developed in the latest
    # debug... we could probably do it all with regex... 
    def __get_volume(self):
        sqn = self.quote_no_maj
        sqnVolume = 0
        temp_quote = ""
        no_volume = True
        hasK = False
        
        # IF YOU HAVE PATRIC"k" and 100k it recognizes only the first k... 
        # That is because of how the find k thing works... it only finds the first k in there... fix this!
        if ('k' in sqn):
            for occurence in self.findOccurrences(sqn, 'k'):
                position = occurence - 1 
                check = sqn[position].isdigit()
                if check == True:
                    hasK = True

        try:
            year_to_delete = re.findall('(\d*\.?\d+) *(year)', sqn)[0][0]
            print(year_to_delete)
            temp_quote = sqn.replace(' ' + str(year_to_delete) + ' ','')  
        except:
            temp_quote = sqn

        frequency1 = collections.Counter(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", temp_quote))['1']
        frequency2 = collections.Counter(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", temp_quote))['2']
        frequencyBH1=len(re.findall(r'[1][Hh][1-2][0-9]', sqn))
        frequencyBH2=len(re.findall(r'[2][Hh][1-2][0-9]', sqn))

        print(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", temp_quote))
        for num in map(float, [num.replace('-', '').replace(' ', '') for num in re.findall(r"[-+]?(?:\d*\.\d+|\d+)", temp_quote)]):
            # Some roundabout logic to find if the number if a year or not
            aYear1 = ((str(int(num)) in list(self.years_voc.keys())))
            aYear2 = ((str(int(num))+" " in list(self.years_voc.keys())))
            aYear=False
            if True in [aYear1, aYear2]:
                aYear = True
            
            # The volume cannot be an integer a year or a price (or a bid or offer price)
            if (num.is_integer()) and (not aYear) and (float(num)!=float(self.new_quote.price)) and (float(num)!=float(self.new_quote.bidPrice)) and (float(num)!=float(self.new_quote.offerPrice)):
                
                # Some more roundabout code to not count the 1 in 1B22 etc. Our logic is that we want to only count those 1's that
                # occur ouside the 1b22 etc. so we only want to count 1 if we have more 1s than 1b22 etc. 
                if num==float('1'):
                    if (frequency1 <= frequencyBH1):
                        print(" I came here")
                        continue
                    else: 
                        pass
                if num==float('2'):
                    if (frequency2 <= frequencyBH2):
                        continue
                    else: 
                        pass

                # Rest of the program... finds k's, gw etc. and returns volume accordingly... 

                no_volume = False
                if ('gw' in sqn):
                    if hasK:
                        sqnVolume = str(int(num) * 1000*1000)
                    else:
                        sqnVolume = str(int(num) *1000)
                elif ('mw' in sqn):
                    if hasK:
                        sqnVolume = str(int(num) * 1000)  
                    else:
                        sqnVolume = str(int(num))
                else:
                    if hasK:
                        sqnVolume = str(int(num) * 1000)
                    else: 
                        sqnVolume = str(int(num))
        if no_volume or sqnVolume == str(2):
            if "PJM" in self.new_quote.proxy or "NEPOOL" in self.new_quote.proxy:
                sqnVolume = str(1)
            else:
                sqnVolume = str(50000)
        self.new_quote.volume= sqnVolume

    def __get_price_old(self): # Working fine... 
        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\,\d+", self.quote_no_maj)]):
            if (num not in self.years_list) & (isinstance(num, float)) & (num < 5):
                self.new_quote.price = str(num)

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\.\d+", self.quote_no_maj)]):
            if (num not in self.years_list) & (isinstance(num, float)) & (num < 5):
                self.new_quote.price = str(num)
                
    def __get_price_old2(self):
        price = 0
        bidPrice = 0
        offerPrice = 0
        sqn = self.quote_no_maj

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\,\d+", sqn)]):
            if (num not in years_list) & (isinstance(num, float)) & (num < 5):
                price = str(num)

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\.\d+", sqn)]):
            if (num not in years_list) & (isinstance(num, float)) & (num < 5):
                price = str(num)
               
    def __get_price(self):
        sqn = self.quote_no_maj
        price = 0
        bidPrice = 0
        offerPrice = 0
        
        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\,\d+", sqn)]):
            if (num not in self.years_list) & (isinstance(num, float)) & (num < 5):
                price = str(num)

        for num in map(float, [w.replace(',', '.') for w in re.findall(r"\d+\.\d+", sqn)]):
            if (num not in self.years_list) & (isinstance(num, float)) & (num < 5):
                price = str(num)



        # If there is a number after a dollar sign... it will be a price... set the first you find to be a price... 
        if len(re.findall(r"\$\s?[-+]?[0-9]*\.?[0-9]+", sqn))>0:
            price = re.findall(r"\$\s?[-+]?[0-9]*\.?[0-9]+", sqn)[0].replace('$','').replace(' ', '')

        # If we have like... $2@$1.5...or $2 @ $5... we deal with that case here... we set the bid and offer prices here... 
        # NOTE: I am going to make the first $ sign compulsory instead of optional because otherwise 1H23 @ $2 is understood as 
        # bid 23 dollars, offer 2 dollars... I can fix this easily later by adding a ? in front of the first $ sign below if need be... 
        if len(re.findall(r"\$[-+]?[0-9]*\.?[0-9]+\s?\@\s?\$[-+]?[0-9]*\.?[0-9]+", sqn))>0:
            bidPrice = re.findall(r"\$?[-+]?[0-9]*\.?[0-9]+\s?\@", sqn)
            offerPrice = re.findall(r"@\s?\$[-+]?[0-9]*\.?[0-9]+", sqn)
            bidPrice = bidPrice[0].replace('@', '').replace('$', '').replace(' ', '')
            offerPrice = offerPrice[0].replace('@', '').replace('$', '').replace(' ','')

        self.new_quote.price=price
        self.new_quote.bidPrice=bidPrice
        self.new_quote.offerPrice=offerPrice


        # If there is a number after a dollar sign... it will be a price... set the first you find to be a price... 
        if len(re.findall(r"\$\s?[-+]?[0-9]*\.?[0-9]+", sqn))>0:
            price = re.findall(r"\$\s?[-+]?[0-9]*\.?[0-9]+", sqn)[0].replace('$','').replace(' ', '')

        # If we have like... $2@$1.5...or $2 @ $5... we deal with that case here... we set the bid and offer prices here... 
        if len(re.findall(r"\$?[-+]?[0-9]*\.?[0-9]+\s?\@\s?\$[-+]?[0-9]*\.?[0-9]+", sqn))>0:
            bidPrice = re.findall(r"\$?[-+]?[0-9]*\.?[0-9]+\s?\@", sqn)
            offerPrice = re.findall(r"@\s?\$[-+]?[0-9]*\.?[0-9]+", sqn)
            bidPrice = bidPrice[0].replace('@', '').replace('$', '').replace(' ', '')
            offerPrice = offerPrice[0].replace('@', '').replace('$', '').replace(' ','')

        print(float(bidPrice))
        print(float(offerPrice))
        self.new_quote.price= price

    def __is_price_request(self):
        self.new_quote.price_request = not (
                any([isinstance(num, float) for num in map(float, [w.replace(',', '.') for w in
                                                                   re.findall(r"\d+\,\d+",
                                                                              self.quote_no_maj)])]) |
                any([isinstance(num, float) for num in map(float, [w.replace(',', '.') for w in
                                                                   re.findall(r"\d+\.\d+",
                                                                              self.quote_no_maj)])])) or 'request' in self.quote_no_maj
    def __get_label(self): #fine
        self.new_quote.label = []
        for key, value in self.label_voc.items():
            if key in self.quote_no_maj:
                if type(self.label_voc[key]) is list:
                    self.new_quote.label.extend(self.label_voc[key])
                else:
                    self.new_quote.label.append(self.label_voc[key])


    def __get_proxy_old(self): #Broken 
        for key, value in self.proxy_voc.items():
            if key in self.quote_no_maj:
                self.new_quote.proxy = self.proxy_voc[key][0:]
                proxy_year = str(self.new_quote.end_year) 
                proxy_year = proxy_year.strip(".0")             
                self.new_quote.proxy = self.new_quote.proxy + ' ' + proxy_year
                return
            
    def myFunc(self,e):
        return len(e)

        # Add the new proxy voc as well... 
    
    def __get_proxy(self): #Broken 
        possibleVals = []
        proxyKeys = list(self.proxy_voc.keys())
        proxyKeys.sort(key=self.myFunc)
        for key in proxyKeys:
            if key in self.quote_no_maj:
                possibleVals.append(key)
                self.new_quote.proxy = self.proxy_voc[key][0:]
                proxy_year = str(self.new_quote.end_year) 
                proxy_year = proxy_year.strip(".0")
                if not self.new_quote.proxy in self.backHalfs:
                        self.new_quote.proxy = self.new_quote.proxy + ' ' + proxy_year
        print(possibleVals)
        return
   
    def __get_registry(self):
        for key,value in self.registry_voc.items():
            if key in self.quote_no_maj:
                self.new_quote.registry = self.registry_voc[key][0:]
                
    # For now we are hardcodingt a default value here... we will make this a bit smarter in the next version
    # Even though we press enter... it does not seem to do the job... 
    def __get_counterparty(self):
        self.new_quote.counterparty = 'Broker'
        for key,value in self.broker_voc.items():
            if key in self.quote_no_maj:
                self.new_quote.counterparty = self.broker_voc[key][0:]
       
class Quote:
    bidPrice: str
    offerPrice: str
    counterparty: str
    price: str
    volume: str
    start_month: str
    end_month: str
    start_year: str
    end_year: str
    support_type: str
    tech: list
    label: list
    origin: list
    broker: str
    quote_type: str
    proxy: str
    registry: str
    raw: str
